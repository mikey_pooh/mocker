use fake::Fake;
use fake::faker::lorem::en::Word;
use reqwest::StatusCode;

use crate::shared::{Anyhow, BASE_ADDR, run, Tests};

const FOLDER: &str = "./examples/matching/path/resources";

mod shared;

#[tokio::test]
async fn paths() -> Anyhow<()> {
    let mut tests = tests();

    run(FOLDER, &mut tests).await?;
    Ok(())
}

fn tests() -> Tests {
    let tests = Tests::new();

    tests.push(Box::pin(concrete()));
    tests.push(Box::pin(segment()));
    tests.push(Box::pin(tail()));
    tests.push(Box::pin(default()));
    tests
}

async fn concrete() -> Anyhow<()> {
    let url = format!("http://{}/base/concrete/path", BASE_ADDR);
    let expected = "Response for all GET requests to /base/concrete/path.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn segment() -> Anyhow<()> {
    let random: String = Word().fake();
    let url = format!("http://{}/base/{}/path", BASE_ADDR, random);
    let expected = "Response for all paths at /base/{any_value}/path.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn tail() -> Anyhow<()> {
    let random: String = Word().fake();
    let url = format!("http://{}/base/{}", BASE_ADDR, random);
    let expected = "Response for all other paths at /base/{any_path}.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn default() -> Anyhow<()> {
    let random: String = Word().fake();
    let url = format!("http://{}/{}", BASE_ADDR, random);
    let expected = "Response for all GET requests not matching more specific rules.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}
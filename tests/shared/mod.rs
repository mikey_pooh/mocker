use std::io::BufReader;
use std::path::PathBuf;
use std::thread;
use futures::future::BoxFuture;
use futures::stream::FuturesUnordered;
use futures::StreamExt;
use serde_json::Value as Json;
use tokio::fs::File;
use tokio::io::AsyncReadExt;
use tokio::runtime::Builder;
use tracing::error;

use mocker::Network;

#[allow(dead_code)] pub const APPLICATION_JSON: &str = "application/json";
#[allow(dead_code)] pub const APPLICATION_XML: &str = "application/xhtml+xml";
pub const BASE_ADDR: &str = "127.0.0.1:8080";

pub type Anyhow<T> = anyhow::Result<T>;
pub type Test = BoxFuture<'static, Anyhow<()>>;
pub type Tests = FuturesUnordered<Test>;

pub async fn run(folder: &str, tests: &mut Tests) -> Anyhow<()> {
    start(folder).await?;
    while let Some(result) = tests.next().await {
        if let Err(err) = result {
            return Err(err);
        }
    }

    Ok(())
}

pub async fn load(folder: &str, file: &str) -> Anyhow<File> {
    let path = format!("{}/{}", folder, file);
    Ok(File::open(path).await?)
}

#[allow(dead_code)]
pub async fn read_to_bytes(mut file: File) -> Anyhow<Vec<u8>> {
    let mut bytes = vec![];

    file.read_to_end(&mut bytes).await?;
    Ok(bytes)
}

#[allow(dead_code)]
pub async fn read_to_json(file: File) -> Anyhow<Json> {
    let file = file.into_std().await;
    let reader = BufReader::new(file);

    Ok(serde_json::from_reader(reader)?)
}

#[allow(dead_code)]
pub fn encode_tuples(json: Json) -> Vec<(String, String)> {
    let mut output = vec![];
    if let Json::Object(map) = json {
        for (key, value) in map {
            if let Some(value) = encoded_value(value) {
                let value = (key, value);

                output.push(value);
            }
        }
    }

    output
}

async fn start(folder: &str) -> Anyhow<()> {
    let compose = load(folder, "mocker-compose.yml").await?.into_std().await;
    let reader = BufReader::new(compose);
    let network: Network = serde_yaml::from_reader(reader)?;
    let base = PathBuf::from(folder);
    let runtime = Builder::new_current_thread().enable_all().build()?;

    thread::spawn(move || {
        for service in runtime.block_on(network.start(base)) {
            if let Err(err) = service {
                error!("Terminating Network. Error running services: {}", err);
            }
        }
    });

    Ok(())
}

fn encoded_value(value: Json) -> Option<String> {
    match value {
        Json::Bool(bool) => Some(bool.to_string()),
        Json::Number(number) => Some(number.to_string()),
        Json::String(value) => Some(value),
        _ => None
    }
}
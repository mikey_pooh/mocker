use reqwest::{Client, StatusCode};

use crate::shared::{Anyhow, APPLICATION_JSON, APPLICATION_XML, BASE_ADDR, run, Tests};

mod shared;

const ACCEPT: &str = "accept";
const FOLDER: &str = "./examples/matching/parameters/advanced/resources";
const TEXT_HTML: &str = "text/html";

#[tokio::test]
async fn advanced() -> Anyhow<()> {
    let mut tests = tests();

    run(FOLDER, &mut tests).await?;
    Ok(())
}

fn tests() -> Tests {
    let tests = Tests::new();

    tests.push(Box::pin(js_validation()));
    tests.push(Box::pin(js_validation_invalid_range()));
    tests.push(Box::pin(js_validation_range_span_too_large()));
    tests.push(Box::pin(all_of()));
    tests.push(Box::pin(all_of_empty()));
    tests.push(Box::pin(all_of_with_string()));
    tests.push(Box::pin(all_of_js_rejected()));
    tests.push(Box::pin(any_of_application_json()));
    tests.push(Box::pin(any_of_starts_with_text_html()));
    tests.push(Box::pin(any_of_default_accept()));
    tests.push(Box::pin(not_found_path()));
    tests
}

async fn js_validation() -> Anyhow<()> {
    let url = format!("http://{}/js/validation?page-range=1-+9999", BASE_ADDR);
    let expected = "Response for /js/validation where the page-range query parameter has a span >= 1 and <= 10000";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn js_validation_invalid_range() -> Anyhow<()> {
    let url = format!("http://{}/js/validation?page-range=0-2", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn js_validation_range_span_too_large() -> Anyhow<()> {
    let url = format!("http://{}/js/validation?page-range=1-+10001", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn all_of() -> Anyhow<()> {
    let url = format!("http://{}/all/of?display=1001", BASE_ADDR);
    let expected = "Response for /all/of when display query is a positive integer >= 1000.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn all_of_empty() -> Anyhow<()> {
    let url = format!("http://{}/all/of?display=", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn all_of_with_string() -> Anyhow<()> {
    let url = format!("http://{}/all/of?display=data", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn all_of_js_rejected() -> Anyhow<()> {
    let url = format!("http://{}/all/of?display=999", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_of_application_json() -> Anyhow<()> {
    let url = format!("http://{}/any/of", BASE_ADDR);
    let expected = "Response for /any/of when accept header is 'application/json' or any value starting with 'text/html'.";
    let client = Client::new().get(url).header(ACCEPT, APPLICATION_JSON);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_of_starts_with_text_html() -> Anyhow<()> {
    let url = format!("http://{}/any/of", BASE_ADDR);
    let expected = "Response for /any/of when accept header is 'application/json' or any value starting with 'text/html'.";
    let accept = format!("{}, {}", TEXT_HTML, APPLICATION_XML);
    let client = Client::new().get(url).header(ACCEPT, accept);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_of_default_accept() -> Anyhow<()> {
    let url = format!("http://{}/any/of", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn not_found_path() -> Anyhow<()> {
    let url = format!("http://{}/default", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::NOT_FOUND);
    Ok(())
}


use reqwest::{Client, StatusCode};
use reqwest::multipart::{Form, Part};
use serde_json::json;

use crate::shared::{Anyhow, APPLICATION_JSON, APPLICATION_XML, BASE_ADDR, load, read_to_json, run, encode_tuples, Tests, read_to_bytes};

mod shared;

const CONTENT_TYPE: &str = "content-type";
const FOLDER: &str = "./examples/matching/body/resources";

#[tokio::test]
async fn body() -> Anyhow<()> {
    let mut tests = tests();

    run(FOLDER, &mut tests).await?;
    Ok(())
}

fn tests() -> Tests {
    let tests = Tests::new();

    tests.push(Box::pin(any_with_json_payload()));
    tests.push(Box::pin(any_with_text_payload()));
    tests.push(Box::pin(any_with_empty_payload()));
    tests.push(Box::pin(any_with_put()));
    tests.push(Box::pin(empty()));
    tests.push(Box::pin(empty_with_payload()));
    tests.push(Box::pin(form_matching()));
    tests.push(Box::pin(form_matching_with_incorrect_content_type()));
    tests.push(Box::pin(file_matching()));
    tests.push(Box::pin(file_non_matching()));
    tests.push(Box::pin(json_matching()));
    tests.push(Box::pin(json_matching_with_incorrect_content_type()));
    tests.push(Box::pin(multipart_matching()));
    tests
}

async fn any_with_json_payload() -> Anyhow<()> {
    let url = format!("http://{}/any", BASE_ADDR);
    let expected = "Response for all POST requests to /any whether a payload is sent or not.";
    let json = json!({ "id": 12345 });
    let client = Client::new().post(url).json(&json);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_with_text_payload() -> Anyhow<()> {
    let url = format!("http://{}/any", BASE_ADDR);
    let expected = "Response for all POST requests to /any whether a payload is sent or not.";
    let client = Client::new().post(url).body(expected);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_with_empty_payload() -> Anyhow<()> {
    let url = format!("http://{}/any", BASE_ADDR);
    let expected = "Response for all POST requests to /any whether a payload is sent or not.";
    let client = Client::new().post(url);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_with_put() -> Anyhow<()> {
    let url = format!("http://{}/any", BASE_ADDR);
    let json = json!({ "id": 12345 });
    let client = Client::new().put(url).json(&json);
    let response = client.send().await?;
    let status = response.status();

    assert_eq!(status, StatusCode::NOT_FOUND);
    Ok(())
}

async fn empty() -> Anyhow<()> {
    let url = format!("http://{}/empty", BASE_ADDR);
    let expected = "Response for all POST requests to /empty where no payload is sent.";
    let client = Client::new().post(url);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn empty_with_payload() -> Anyhow<()> {
    let url = format!("http://{}/empty", BASE_ADDR);
    let json = json!({ "id": 12345 });
    let client = Client::new().post(url).json(&json);
    let response = client.send().await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn form_matching() -> Anyhow<()> {
    let url = format!("http://{}/form", BASE_ADDR);
    let expected = "Response for all POST requests to /form where encoded payload, when parsed to json, equals the content of supplied file.";
    let file = load(FOLDER, "chaucer.json").await?;
    let json = read_to_json(file).await?;
    let encoded = encode_tuples(json);
    let client = Client::new().post(url).form(&encoded);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn form_matching_with_incorrect_content_type() -> Anyhow<()> {
    let url = format!("http://{}/form", BASE_ADDR);
    let expected = "Response for all POST requests to /form where encoded payload, when parsed to json, equals the content of supplied file.";
    let file = load(FOLDER, "chaucer.json").await?;
    let json = read_to_json(file).await?;
    let encoded = encode_tuples(json);
    let client = Client::new().post(url).form(&encoded).header(CONTENT_TYPE, APPLICATION_XML);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn file_matching() -> Anyhow<()> {
    let url = format!("http://{}/file", BASE_ADDR);
    let expected = "Response for all PUT requests to /file where payload exactly equal to the content of aforementioned file.";
    let file = load(FOLDER, "mocker-compose.yml").await?;
    let client = Client::new().put(url).body(file);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn file_non_matching() -> Anyhow<()> {
    let url = format!("http://{}/file", BASE_ADDR);
    let file = load(FOLDER, "chaucer.json").await?;
    let client = Client::new().put(url).body(file);
    let response = client.send().await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn json_matching() -> Anyhow<()> {
    let url = format!("http://{}/json", BASE_ADDR);
    let expected = "Response for all POST requests to /json where json payload equals the content of supplied file.";
    let file = load(FOLDER, "chaucer.json").await?;
    let client = Client::new().post(url).body(file).header(CONTENT_TYPE, APPLICATION_JSON);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn json_matching_with_incorrect_content_type() -> Anyhow<()> {
    let url = format!("http://{}/json", BASE_ADDR);
    let expected = "Response for all POST requests to /json where json payload equals the content of supplied file.";
    let file = load(FOLDER, "chaucer.json").await?;
    let client = Client::new().post(url).body(file).header(CONTENT_TYPE, APPLICATION_XML);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn multipart_matching() -> Anyhow<()> {
    let url = format!("http://{}/multipart", BASE_ADDR);
    let expected = "Response for all POST requests to /multipart where payload matches by content-type.";
    let filename = "schema.json";
    let file = load(FOLDER, filename).await?;
    let bytes = read_to_bytes(file).await?;
    let upload = Part::bytes(bytes);
    let form = Form::new().text("title", filename).part("upload", upload);
    let client = Client::new().post(url).multipart(form);
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}
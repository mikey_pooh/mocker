use reqwest::{Client, StatusCode};
use uuid::Uuid;

use crate::shared::{Anyhow, BASE_ADDR, run, Tests};

mod shared;

const ACCEPT_LANGUAGE: &str = "accept-language";
const FOLDER: &str = "./examples/matching/parameters/basics/resources";
const SILLY: &str = "silly-header";

#[tokio::test]
async fn basics() -> Anyhow<()> {
    let mut tests = tests();

    run(FOLDER, &mut tests).await?;
    Ok(())
}

fn tests() -> Tests {
    let tests = Tests::new();

    tests.push(Box::pin(any()));
    tests.push(Box::pin(any_without_display_query()));
    tests.push(Box::pin(any_without_silly_header()));
    tests.push(Box::pin(any_integer()));
    tests.push(Box::pin(any_integer_with_empty()));
    tests.push(Box::pin(any_integer_with_string()));
    tests.push(Box::pin(any_string()));
    tests.push(Box::pin(any_string_with_empty()));
    tests.push(Box::pin(any_string_with_integer()));
    tests.push(Box::pin(any_uuid()));
    tests.push(Box::pin(any_uuid_with_empty()));
    tests.push(Box::pin(any_uuid_with_string()));
    tests.push(Box::pin(not_present()));
    tests.push(Box::pin(not_present_with_key()));
    tests.push(Box::pin(not_present_with_empty_key()));
    tests.push(Box::pin(optional_with_param()));
    tests.push(Box::pin(optional_without_param()));
    tests.push(Box::pin(optional_with_non_matching_param()));
    tests.push(Box::pin(fixed_matching()));
    tests.push(Box::pin(fixed_non_matching()));
    tests.push(Box::pin(fixed_missing()));
    tests.push(Box::pin(starts_with_matching()));
    tests.push(Box::pin(starts_with_non_matching()));
    tests.push(Box::pin(starts_with_not_present()));
    tests.push(Box::pin(not_found_path()));
    tests
}

async fn any() -> Anyhow<()> {
    let url = format!("http://{}/any?display=12345", BASE_ADDR);
    let expected = "Response for /any where the display query parameter and a silly-header are present.";
    let client = Client::new().get(url).header(SILLY, "daft/value");
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_without_display_query() -> Anyhow<()> {
    let url = format!("http://{}/any?id=12345", BASE_ADDR);
    let client = Client::new().get(url).header(SILLY, "daft/value");
    let response = client.send().await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_without_silly_header() -> Anyhow<()> {
    let url = format!("http://{}/any?display=12345", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_integer() -> Anyhow<()> {
    let url = format!("http://{}/any/integer?display=12345", BASE_ADDR);
    let expected = "Response for /any/integer where the display query parameter is an integer.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_integer_with_empty() -> Anyhow<()> {
    let url = format!("http://{}/any/integer?display=", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_integer_with_string() -> Anyhow<()> {
    let url = format!("http://{}/any/integer?display=someData", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_string() -> Anyhow<()> {
    let url = format!("http://{}/any/string?display=someData", BASE_ADDR);
    let expected = "Response for /any/string where the display query parameter is present, non-empty and not an integer.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_string_with_empty() -> Anyhow<()> {
    let url = format!("http://{}/any/string?display=", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_string_with_integer() -> Anyhow<()> {
    let url = format!("http://{}/any/string?display=54321", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_uuid() -> Anyhow<()> {
    let uuid = Uuid::new_v4();
    let url = format!("http://{}/any/uuid?display={}", BASE_ADDR, uuid);
    let expected = "Response for /any/uuid where the display query parameter is a UUID (v4).";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn any_uuid_with_empty() -> Anyhow<()> {
    let url = format!("http://{}/any/uuid?display=", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn any_uuid_with_string() -> Anyhow<()> {
    let url = format!("http://{}/any/uuid?display=someData", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn not_present() -> Anyhow<()> {
    let url = format!("http://{}/not/present?display=true&id=54321&valid=false", BASE_ADDR);
    let expected = "Response for /not/present when key query is not present.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn not_present_with_key() -> Anyhow<()> {
    let url = format!("http://{}/not/present?key=present", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn not_present_with_empty_key() -> Anyhow<()> {
    let url = format!("http://{}/not/present?key=", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn optional_with_param() -> Anyhow<()> {
    let url = format!("http://{}/optional?id=12345", BASE_ADDR);
    let expected = "Response for /optional when id query parameter, if it is present, must equal 12345.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn optional_without_param() -> Anyhow<()> {
    let url = format!("http://{}/optional?valid=false", BASE_ADDR);
    let expected = "Response for /optional when id query parameter, if it is present, must equal 12345.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn optional_with_non_matching_param() -> Anyhow<()> {
    let url = format!("http://{}/optional?id=54321", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn fixed_matching() -> Anyhow<()> {
    let url = format!("http://{}/fixed?page=5", BASE_ADDR);
    let expected = "Response for /fixed when page query is 5.";
    let response = reqwest::get(url).await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn fixed_non_matching() -> Anyhow<()> {
    let url = format!("http://{}/fixed?page=2", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn fixed_missing() -> Anyhow<()> {
    let url = format!("http://{}/fixed?id=5&valid=false", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn starts_with_matching() -> Anyhow<()> {
    let url = format!("http://{}/starts/with", BASE_ADDR);
    let expected = "Response for /starts/with when accept-language request header starts with 'en-US'.";
    let client = Client::new().get(url).header(ACCEPT_LANGUAGE, "en-US,en;q=0.5");
    let response = client.send().await?;
    let status = response.status();
    let text = response.text().await?;

    assert_eq!(status, StatusCode::OK);
    assert_eq!(text, expected);
    Ok(())
}

async fn starts_with_non_matching() -> Anyhow<()> {
    let url = format!("http://{}/starts/with", BASE_ADDR);
    let client = Client::new().get(url).header(ACCEPT_LANGUAGE, "en-UK,en;q=0.5");
    let response = client.send().await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn starts_with_not_present() -> Anyhow<()> {
    let url = format!("http://{}/starts/with", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::BAD_REQUEST);
    Ok(())
}

async fn not_found_path() -> Anyhow<()> {
    let url = format!("http://{}/default", BASE_ADDR);
    let response = reqwest::get(url).await?;
    let status = response.status();

    assert_eq!(status, StatusCode::NOT_FOUND);
    Ok(())
}


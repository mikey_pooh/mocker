use std::env::current_dir;
use std::fs::{File, read_dir};
use std::io::BufReader;
use std::path::{Path, PathBuf};

use anyhow::anyhow;
use structopt::StructOpt;

use crate::Anyhow;
use crate::Network;

const COMPOSE: &str = "mocker-compose.yml";
const NO_COMPOSE_FILE: &str = "No compose file available to build network.";

#[derive(Debug, StructOpt)]
pub struct Args {
    #[structopt(parse(from_os_str))]
    folder: Option<PathBuf>
}

impl Args {
    pub fn compose(&mut self) -> Anyhow<(PathBuf, Network)> {
        let base = match self.folder.take() {
            Some(folder) => folder,
            None => current_dir()?
        };

        let file = self.file(&base)?;
        let reader = BufReader::new(file);
        Ok((base, serde_yaml::from_reader(reader)?))
    }

    fn file(&mut self, base: &Path) -> Anyhow<File> {
        let mut dir = read_dir(base)?;
        while let Some(Ok(entry)) = dir.next() {
            let filename = entry.file_name();
            if filename.eq_ignore_ascii_case(COMPOSE) {
                return Ok(File::open(entry.path())?);
            }
        }

        Err(anyhow!("{}", NO_COMPOSE_FILE))
    }
}
use actix_web::dev::{ServiceRequest, ServiceResponse};
use actix_web::Error;
use actix_web::web::Data;
use tracing::{info_span, Span};
use tracing_actix_web::RootSpanBuilder;
use tracing_subscriber::filter::LevelFilter;
use tracing_subscriber::fmt;
use tracing_subscriber::fmt::format::FmtSpan;
use tracing_subscriber::util::SubscriberInitExt;
use uuid::Uuid;

use crate::{INTEGRITY_ERROR, ServiceConfig};

pub(super) struct TracingSpan;

impl RootSpanBuilder for TracingSpan {
    fn on_request_start(request: &ServiceRequest) -> Span {
        let data: &Data<ServiceConfig> = request.app_data().expect(INTEGRITY_ERROR);
        let service = &data.name;
        let id = Uuid::new_v4().to_string();
        let path = TracingSpan::path(request);
        info_span!("Request", service, id, path)
    }

    fn on_request_end<B>(_: Span, _: &Result<ServiceResponse<B>, Error>) { }
}

impl TracingSpan {
    fn path(request: &ServiceRequest) -> String {
        let method = request.method();
        let queries = request.query_string();
        if queries.is_empty() {
            return format!("{}{}", method, request.path());
        }

        format!("{}{}?{}", method, request.path(), queries)
    }
}

pub(super) fn start_logging(filter: LevelFilter) {
    let formatter = fmt();
    formatter.with_max_level(filter)
        .with_writer(std::io::stdout)
        .with_line_number(false)
        .with_thread_ids(false)
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_target(false)
        .finish()
        .init();
}
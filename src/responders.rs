#![allow(clippy::mutable_key_type)]

use std::collections::HashMap;
use std::mem;
use std::path::Path;
use std::str::FromStr;
use std::sync::Arc;

use actix_web::{HttpResponse, HttpResponseBuilder};
use actix_web::http::header::{HeaderMap, HeaderName, HeaderValue};
use actix_web::http::StatusCode;
use actix_web::web::Bytes;
use format_bytes::format_bytes;
use reqwest::{Response, Url};
use serde::{Deserialize, Deserializer};
use serde::de::Error;
use serde_json::{from_slice, from_str, Map, to_vec};

use crate::{
    Anyhow,
    BODY,
    FilePath,
    Generation,
    GenerationBody,
    HeaderKey,
    HEADERS,
    Json,
    REF_COUNT_ERROR,
    Registry,
    Response as OutgoingResponse,
    ResponseScope,
    SCRIPT_ERROR,
    scripting::JsReply,
    ScriptLockType,
    SERIALISATION_ERROR,
    Status,
    STATUS,
    Transformed
};

#[derive(Clone, Debug)]
pub(super) enum Body {
    Empty,
    Bytes(Bytes),
    Generator,
    Json {
        bytes: Bytes,
        json: Arc<Json>
    },
    Template(String),
    Text(Bytes)
}

impl Body {
    async fn generate(&self, lock: ScriptLockType, scope: &mut ResponseScope<'_>) -> GenerationBody {
        Ok(match self {
            Self::Empty => OutgoingBody::Empty,
            Self::Generator => return Self::scripted(lock, scope).await,
            Self::Json { bytes, json } => Self::json(bytes, json),
            Self::Template(name) => return Self::template(name, scope).await,
            Self::Bytes(bytes) | Self::Text(bytes) => Self::bytes(bytes),
        })
    }

    fn bytes(bytes: &Bytes) -> OutgoingBody {
        OutgoingBody::Bytes(Bytes::clone(bytes))
    }

    fn json(bytes: &Bytes, json: &Arc<Json>) -> OutgoingBody {
        let bytes = Bytes::clone(bytes);
        let param = Arc::clone(json);
        OutgoingBody::Json { bytes, param }
    }

    async fn scripted(lock: ScriptLockType, scope: &mut ResponseScope<'_>) -> GenerationBody {
        let param = Arc::clone(&scope.request.json);
        if let JsReply::Json(data) = scope.generate(param, lock).await? {
            let bytes = to_vec(&data).expect(SERIALISATION_ERROR).into();
            return Ok(OutgoingBody::Json { bytes, param: Arc::new(data) });
        }

        panic!("{}", SCRIPT_ERROR)
    }

    async fn template(name: &str, scope: &mut ResponseScope<'_>) -> GenerationBody {
        let value = &scope.request.json;
        let request = to_vec(value).expect(SERIALISATION_ERROR);
        let data = if let Some(state) = scope.state.bytes().await {
            format_bytes!(b"{{\"request\":{},\"state\":{}}}", request, state)
        } else {
            format_bytes!(b"{{\"request\":{}}}", request)
        };

        let result = scope.registry.render(name, &data)?;
        if let Ok(data) = from_str::<Json>(&result) {
            return Ok(OutgoingBody::Json {
                bytes: Bytes::from(result),
                param: Arc::new(data)
            });
        }

        Ok(OutgoingBody::Bytes(Bytes::from(result)))
    }
}

#[derive(Clone, Debug)]
pub(super) struct Generator {
    status: StatusCode,
    headers: HashMap<HeaderKey, Header>,
    body: Body
}

impl Generator {
    async fn respond(&self, lock: ScriptLockType, scope: &mut ResponseScope<'_>) -> Generation {
        let status = self.status;
        let capacity = self.headers.len();
        let mut headers = HeaderMap::with_capacity(capacity);
        for (key, value) in &self.headers {
            let k = HeaderName::clone(&key.0);
            let v = HeaderValue::clone(&value.0);

            headers.insert(k, v);
        }

        let body = self.body.generate(lock, scope).await?;
        let outgoing = Outgoing { status, headers, body };
        Ok(MockerResponse::Resolved(outgoing))
    }
}

#[derive(Debug)]
pub(super) enum MockerResponse {
    Resolved(Outgoing),
    Streaming(Response),
    Transformed(Transformed)
}

impl From<MockerResponse> for OutgoingResponse {
    fn from(response: MockerResponse) -> Self {
        match response {
            MockerResponse::Resolved(outgoing) => Ok(outgoing.into()),
            MockerResponse::Transformed(transformed) => transformed.into(),
            MockerResponse::Streaming(response) => {
                let status = response.status();
                let mut builder = HttpResponseBuilder::new(status);
                for (key, value) in response.headers() {
                    let k = HeaderName::clone(key);
                    let v = HeaderValue::clone(value);
                    let header = (k, v);

                    builder.insert_header(header);
                }

                let stream = response.bytes_stream();
                Ok(builder.streaming(stream))
            }
        }
    }
}

impl MockerResponse {
    pub(super) fn as_outgoing_mut(&mut self) -> Option<&mut Outgoing> {
        if let Self::Resolved(ref mut outgoing) = self {
            return Some(outgoing);
        }

        None
    }
}

#[derive(Debug)]
pub(super) struct Outgoing {
    status: StatusCode,
    headers: HeaderMap,
    body: OutgoingBody
}

impl From<Outgoing> for HttpResponse {
    fn from(outgoing: Outgoing) -> Self {
        let mut builder = HttpResponseBuilder::new(outgoing.status);
        for (name, value) in outgoing.headers {
            let header = (name, value);
            builder.insert_header(header);
        }

        let body: Option<Bytes> = outgoing.body.into();
        if let Some(body) = body {
            builder.body(body)
        } else {
            builder.finish()
        }
    }
}

impl From<&mut Outgoing> for Json {
    fn from(outgoing: &mut Outgoing) -> Self {
        let mut json = Map::new();
        let mut headers = Map::new();
        for (key, header) in &outgoing.headers {
            if let Ok(header) = header.to_str() {
                let k = key.to_string();
                let v = Json::String(header.to_string());

                headers.insert(k, v);
            }
        }

        if let Some(body) = outgoing.body.take_json_body() {
            json.insert(BODY.to_string(), body);
        }

        json.insert(HEADERS.to_string(), headers.into());
        json.insert(STATUS.to_string(), Json::from(outgoing.status.as_u16()));
        json.into()
    }
}

impl Outgoing {
    pub(super) fn new(status: StatusCode, headers: HeaderMap, body: OutgoingBody) -> Self {
        Self { status, headers, body }
    }

    async fn async_try_from(response: Response) -> Anyhow<Self> {
        let status = response.status();
        let response_headers = response.headers();
        let capacity = response_headers.len();
        let mut headers = HeaderMap::with_capacity(capacity);
        for (key, value) in response_headers {
            let k = HeaderName::clone(key);
            let v = HeaderValue::clone(value);

            headers.insert(k, v);
        }

        let body = OutgoingBody::async_try_from(response).await?;
        Ok(Outgoing { status, headers, body })
    }
}

#[derive(Debug)]
pub(super) enum OutgoingBody {
    Bytes(Bytes),
    Empty,
    Json {
        bytes: Bytes,
        param: Arc<Json>
    }
}

impl OutgoingBody {
    async fn async_try_from(response: Response) -> Anyhow<Self> {
        if let Some(bytes) = Self::take_response_body(response).await? {
            if let Ok(json) = from_slice::<Json>(&bytes) {
                let param = Arc::new(json);
                return Ok(Self::Json { bytes, param })
            }

            return Ok(Self::Bytes(bytes));
        }

        Ok(Self::Empty)
    }

    fn take_json_body(&mut self) -> Option<Json> {
        if let Self::Json { param, bytes } = self {
            let param = mem::take(param);
            let bytes = mem::take(bytes);
            let json = Arc::try_unwrap(param).expect(REF_COUNT_ERROR);

            *self = Self::Bytes(bytes);
            return Some(json);
        }

        None
    }

    async fn take_response_body(response: Response) -> Anyhow<Option<Bytes>> {
        let bytes = response.bytes().await?;
        Ok(if bytes.is_empty() {
            None
        } else {
            Some(bytes)
        })
    }
}

impl From<OutgoingBody> for Option<Bytes> {
    fn from(outgoing: OutgoingBody) -> Self {
        match outgoing {
            OutgoingBody::Bytes(bytes) | OutgoingBody::Json { bytes, .. } => Some(bytes),
            _ => None
        }
    }
}

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
#[serde(deny_unknown_fields)]
pub(super) struct Mock {
    #[serde(default)]
    status: Status,
    #[serde(default)]
    headers: ResponseHeaders,
    #[serde(default)]
    body: ResponseBody
}

impl Mock {
    pub(super) async fn try_map(self, name: &str, base: &Path, registry: &mut Registry) -> Anyhow<Generator> {
        let status = self.status.0;
        let headers = self.headers.0;
        let body = self.body.try_map(name, base, registry).await?;
        Ok(Generator { status, headers, body })
    }
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq)]
#[serde(deny_unknown_fields)]
pub(super) struct Passthrough {
    #[serde(deserialize_with = "deserialize_url")]
    url: Url,
    then: Option<String>
}

fn deserialize_url<'de, D>(deserializer: D) -> Result<Url, D::Error>
    where D: Deserializer<'de> {
    let raw = String::deserialize(deserializer)?;
    Url::from_str(&raw).map_err(D::Error::custom)
}

impl Passthrough {
    async fn respond(&self, lock: ScriptLockType, scope: &mut ResponseScope<'_>) -> Generation {
        let base = self.url.as_str();
        let reqwest = scope.request.reqwest(base)?;
        let response = scope.client.send(reqwest).await?;
        if self.then.is_some() || lock.is_after() {
            Ok(MockerResponse::Resolved(Outgoing::async_try_from(response).await?))
        } else {
            Ok(MockerResponse::Streaming(response))
        }
    }
}

#[derive(Debug, Deserialize, PartialEq, Eq)]
#[serde(untagged)]
pub(super) enum ResponderBuilder {
    Mock(Mock),
    Passthrough(Passthrough)
}

impl Default for ResponderBuilder {
    fn default() -> Self {
        Self::Mock(Mock::default())
    }
}

impl ResponderBuilder {
    pub(super) async fn try_map(self, name: &str, base: &Path, registry: &mut Registry) -> Anyhow<Responder> {
        Ok(match self {
            Self::Mock(mock) =>
                Responder::Generator(mock.try_map(name, base, registry).await?),
            Self::Passthrough(passthrough) =>
                Responder::Passthrough(passthrough),
        })
    }
}

#[derive(Clone, Debug)]
pub(super) enum Responder {
    Generator(Generator),
    Passthrough(Passthrough)
}

impl Responder {
    pub(super) async fn respond(&self, lock: ScriptLockType, scope: &mut ResponseScope<'_>) -> Generation {
        match self {
            Self::Generator(generator) => generator.respond(lock, scope).await,
            Self::Passthrough(passthrough) => passthrough.respond(lock, scope).await
        }
    }

    pub(super) fn transformer(&self) -> Option<&str> {
        if let Self::Passthrough(passthrough) = self {
            return passthrough.then.as_deref();
        }

        None
    }
}

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "lowercase")]
pub(super) enum ResponseBody {
    #[default]
    Empty,
    File(FilePath),
    Generator,
    Json(FilePath),
    Template(FilePath),
    Text(String)
}

impl ResponseBody {
    pub(super) async fn try_map(self, name: &str, base: &Path, registry: &mut Registry) -> Anyhow<Body> {
        Ok(match self {
            Self::Empty => Body::Empty,
            Self::File(filepath) => Body::Bytes(filepath.load_bytes(base).await?.into()),
            Self::Generator => Body::Generator,
            Self::Template(filepath) => return Self::template(name, base, registry, filepath).await,
            Self::Text(text) => Body::Text(text.into()),
            Self::Json(filepath) => {
                let data = filepath.load_json(base).await?;
                let bytes = to_vec(&data)?.into();
                Body::Json { bytes, json: Arc::new(data) }
            }
        })
    }

    async fn template(name: &str, base: &Path, registry: &mut Registry, filepath: FilePath) -> Anyhow<Body> {
        let tpl_str = filepath.load_string(base).await?;

        registry.register_template_string(name, &tpl_str)?;
        Ok(Body::Template(name.to_string()))
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct Header(HeaderValue);

impl<'de> Deserialize<'de> for Header {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
        where D: Deserializer<'de> {
        let value = String::deserialize(deserializer)?;
        Ok(Self(HeaderValue::from_str(&value).map_err(D::Error::custom)?))
    }
}

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
struct ResponseHeaders(HashMap<HeaderKey, Header>);
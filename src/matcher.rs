#![allow(clippy::mutable_key_type)]
use std::borrow::Cow;
use std::collections::HashMap;
use std::path::Path;
use std::str::FromStr;

use actix_web::http::Method;
use actix_web::web::Bytes;
use anyhow::{anyhow, Error as AnyError, Result as Anyhow};
use async_recursion::async_recursion;
use derivative::Derivative;
use jsonschema::JSONSchema;
use serde::{Deserialize, Deserializer};
use serde::de::Error;
use serde_yaml::{Mapping, Sequence};
use tracing::info;
use uuid::Uuid;

use crate::{Body, ContentType, endpoint::EndpointDetails, FilePath, HeaderKey, IncomingBody, IncomingMultipartBody, INTEGRITY_ERROR, Json, RequestScope, Validation, Yml};

const EMPTY: &str = "";
const INTEGER: &str = "integer";
const INVALID_MAPPING: &str = "Invalid parameter. Expected one of [all_of, any, any_of, not_present, type].";
const INVALID_TYPE: &str = "Invalid type. Expected one of [integer, string, uuid].";
const INVALID_WILDCARD_PATH: &str = "Invalid wildcard path. See 'https://actix.rs/docs/url-dispatch/'";
const PARAMS_NOT_FROM_SEQUENCES: &str = "Invalid sequence. Use all_of or any_of to create multiple matching rules.";
const PATHS_START_WITH_SLASH: &str = "Paths must start with '/'. See 'https://actix.rs/docs/url-dispatch/'";
const STRING: &str = "string";
const UNKNOWN_METHOD: &str = "Invalid request method. Expected one of [DELETE, GET, HEAD, OPTIONS, PATCH, POST, PUT]";
const UNPARSEABLE_METHOD: &str = "Could not parse request method/methods. Single methods should be expressed with the method name. Multiple methods should be expressed as a sequence.";
const UNRESOLVABLE_PATH: &str = "Path unresolvable due to asset clash.";
const UUID: &str = "uuid";

#[derive(Debug)]
pub(super) struct Matcher {
    queries: Queries,
    headers: RequestHeaders,
    body: MatcherBody
}

impl Matcher {
    pub(super) async fn matches(&self, endpoint: &str, scope: &mut RequestScope<'_>, body: &mut IncomingBody<'_>) -> Validation {
        #[allow(clippy::collapsible_if)]
        if self.queries.matches(endpoint, scope).await? {
            if self.headers.matches(endpoint, scope).await? {
                let content_type = self.body.as_content_type();

                body.resolve(content_type).await?;
                return self.body.matches(endpoint, scope, body).await;
            }
        }

        Ok(false)
    }
}

#[derive(Debug, Default, Deserialize)]
#[serde(deny_unknown_fields)]
pub(super) struct RequestMatcher {
    #[serde(default)]
    path: HttpPath,
    #[serde(default)]
    #[serde(rename = "method")]
    methods: HttpMethods,
    #[serde(default)]
    queries: Queries,
    #[serde(default)]
    headers: RequestHeaders,
    #[serde(default)]
    body: RequestBody
}

impl RequestMatcher {
    pub(super) async fn try_map(self, disallowed: Option<&str>, base: &Path) -> Anyhow<Vec<(EndpointDetails, Matcher)>> {
        let path = self.path;
        if let Some(disallowed) = disallowed {
            if path.0.starts_with(disallowed) {
                return Err(anyhow!(UNRESOLVABLE_PATH));
            }
        }

        let body = self.body.try_map(base).await?;
        let mut matchers = vec![];
        // TODO: For the most part, there will only be one method, making many of these clones unnecessary in the general case
        // TODO: Use a trait to abstract away this unnecessary cloning where possible
        for method in self.methods.0 {
            let path = HttpPath::clone(&path);
            let method = method.0;
            let details = EndpointDetails::new(path.0, method);
            let queries = Queries::clone(&self.queries);
            let headers = RequestHeaders::clone(&self.headers);
            let body = MatcherBody::clone(&body);
            let matcher = Matcher { queries, headers, body };

            matchers.push((details, matcher));
        }


        Ok(matchers)
    }
}

#[derive(Clone, Debug)]
struct HttpPath(String);

impl Default for HttpPath {
    fn default() -> Self {
        Self("/{path:.*}".to_string())
    }
}

impl<'de> Deserialize<'de> for HttpPath {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> {
        let path = String::deserialize(deserializer)?;
        if path.starts_with('/') {
            let segments = path.split('/');
            for segment in segments {
                if contains_wild_chars(segment) {
                    if segment.starts_with('{') && segment.ends_with('}') {
                        let path = &segment[1..segment.len() - 1];
                        if valid(path) {
                            continue;
                        }
                    }

                    return Err(D::Error::custom(INVALID_WILDCARD_PATH));
                }
            }

            return Ok(Self(path));
        }

        Err(D::Error::custom(PATHS_START_WITH_SLASH))
    }
}

#[derive(Debug)]
struct HttpMethod(Method);

impl Default for HttpMethod {
    fn default() -> Self {
        Self(Method::GET)
    }
}

impl TryFrom<&str> for HttpMethod {
    type Error = AnyError;

    fn try_from(method: &str) -> Result<Self, Self::Error> {
        Ok(Self(match method {
            "DELETE" => Method::DELETE,
            "GET" => Method::GET,
            "HEAD" => Method::HEAD,
            "OPTIONS" => Method::OPTIONS,
            "PATCH" => Method::PATCH,
            "POST" => Method::POST,
            "PUT" => Method::PUT,
            _ => return Err(anyhow!(UNKNOWN_METHOD))
        }))
    }
}

#[derive(Debug)]
struct HttpMethods(Vec<HttpMethod>);

impl Default for HttpMethods {
    fn default() -> Self {
        Self(vec![HttpMethod::default()])
    }
}

impl TryFrom<&Sequence> for HttpMethods {
    type Error = AnyError;

    fn try_from(sequence: &Sequence) -> Result<Self, Self::Error> {
        let mut methods = vec![];
        for value in sequence {
            if let Yml::String(method) = value {
                let method = HttpMethod::try_from(method.as_str())?;

                methods.push(method);
                continue;
            }

            return Err(anyhow!(UNPARSEABLE_METHOD));
        }

        Ok(Self(methods))
    }
}

impl TryFrom<&str> for HttpMethods {
    type Error = AnyError;

    fn try_from(method: &str) -> Result<Self, Self::Error> {
        Ok(Self(vec![HttpMethod::try_from(method)?]))
    }
}

impl<'de> Deserialize<'de> for HttpMethods {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> {
        let result = match Yml::deserialize(deserializer)? {
            Yml::String(method) => Self::try_from(&*method),
            Yml::Sequence(sequence) => Self::try_from(&sequence),
            _ => return Err(D::Error::custom(UNPARSEABLE_METHOD))
        };

        result.map_err(D::Error::custom)
    }
}

#[derive(Debug, Derivative)]
#[derivative(PartialEq, Eq)]
struct JSchema {
    schema: Json,
    #[derivative(PartialEq="ignore")]
    compiled: JSONSchema
}

impl Clone for JSchema {
    fn clone(&self) -> Self {
        let schema = Json::clone(&self.schema);
        Self::try_from(schema).expect(INTEGRITY_ERROR)
    }
}

impl JSchema {
    fn matches(&self, json: &Json) -> bool {
        self.compiled.is_valid(json)
    }
}

impl TryFrom<Json> for JSchema {
    type Error = AnyError;

    fn try_from(schema: Json) -> Result<Self, Self::Error> {
        match JSONSchema::compile(&schema) {
            Ok(compiled) => Ok(Self { schema, compiled }),
            Err(err) => Err(anyhow!("{}", err))
        }
    }
}

#[derive(Clone, Debug)]
enum MatcherBody {
    Any,
    Bytes(Bytes),
    Empty,
    Form(JsonMatcherBody),
    Json(JsonMatcherBody),
    Multipart(MultipartMatcherBody)
}

impl MatcherBody {
    fn as_content_type(&self) -> Option<ContentType> {
        match self {
            Self::Any | Self::Empty => None,
            Self::Bytes(_) => Some(ContentType::OctetStream),
            Self::Form(_) => Some(ContentType::UrlEncoded),
            Self::Json(_) => Some(ContentType::ApplicationJson),
            Self::Multipart(body) => {
                let mut types = HashMap::new();
                for (key, value) in &body.0 {
                    if let Some(field) = value {
                        let content_type = ContentType::from(field);
                        let key = String::clone(key);

                        types.insert(key, content_type);
                    }
                }
                Some(ContentType::Multipart(types))
            }
        }
    }

    async fn matches(&self, endpoint: &str, scope: &mut RequestScope<'_>, body: &mut IncomingBody<'_>) -> Validation {
        match self {
            Self::Any => Ok(true),
            Self::Empty => Ok(Self::match_empty(body)),
            Self::Bytes(bytes) => Ok(Self::match_bytes(bytes, body)),
            Self::Multipart(matcher) => matcher.matches(endpoint, scope, body).await,
            Self::Form(matcher) | Self::Json(matcher) => {
                matcher.matches(endpoint, scope, body).await
            }
        }
    }

    fn match_bytes<B>(bytes: &[u8], body: &mut B) -> bool
    where B: Body {
        let message = match body.resolve_to_bytes() {
            Some(incoming) => {
                if bytes.eq(&incoming) {
                    return true;
                }

                "Payload does not match expectation"
            },
            None => "No payload"
        };

        info!("Byte body matching failed. {}", message);
        false
    }

    fn match_empty(body: &mut IncomingBody<'_>) -> bool {
        if body.is_empty() {
            return true;
        }

        info!("Empty body matching failed. Payload found");
        false
    }
}

#[derive(Clone, Debug)]
enum JsonMatcherBody {
    Fixed(Json),
    Schema(JSchema),
    Validated(String)
}

impl JsonMatcherBody {
    async fn matches<B>(&self, endpoint: &str, scope: &mut RequestScope<'_>, body: &mut B) -> Validation
    where B: Body {
        Ok(match self {
            Self::Fixed(json) => Self::match_fixed(json, body),
            Self::Schema(schema) => Self::match_schema(schema, body),
            Self::Validated(validator) => {
                let param = body.resolve_to_param();
                if scope.validate(validator, endpoint, param).await? {
                    return Ok(true);
                }

                info!("Validated body matching failed. Validation function returned false.");
                false
            }
        })
    }

    fn match_fixed<B>(json: &Json, body: &mut B) -> bool
    where B: Body {
        let message: Cow<str> = match body.resolve_to_json() {
            Some(incoming) => {
                if json.eq(&*incoming) {
                    return true;
                }

                let message = format!("Expected {}. Received {}", json, incoming);
                message.into()
            },
            None => "No compatible payload".into()
        };

        info!("Json body matching failed. {}", message);
        false
    }

    fn match_schema<B>(schema: &JSchema, body: &mut B) -> bool
    where B: Body {
        let message: Cow<str> = match body.resolve_to_json() {
            Some(incoming) => {
                if schema.matches(&incoming) {
                    return true;
                }

                let message = format!("Incoming: {}", incoming);
                message.into()
            },
            None => "No compatible payload".into()
        };

        info!("Schema validation failed. {}", message);
        false
    }
}

#[derive(Clone, Debug)]
struct MultipartMatcherBody(HashMap<String, Option<MultipartMatcherField>>);

impl MultipartMatcherBody {
    async fn matches(&self, endpoint: &str, scope: &mut RequestScope<'_>, body: &mut IncomingBody<'_>) -> Validation {
        if let IncomingBody::Multipart { parts, .. } = body {
            for (key, value) in &self.0 {
                info!("Matching multipart field: '{}'", key);
                match parts.get_mut(key) {
                    Some(body) => {
                        if let Some(value) = value {
                            let matches = value.matches(endpoint, scope, body).await?;
                            if ! matches {
                                info!("Matching failed for multipart field: '{}'", key);
                                return Ok(false);
                            }
                        }

                        continue;
                    },
                    None => {
                        info!("Matching failed for multipart field: '{}'. No corresponding field.", key);
                        return Ok(false);
                    }
                }
            }

            return Ok(true);
        }

        Ok(false)
    }
}

#[derive(Clone, Debug)]
enum MultipartMatcherField {
    Bytes(Bytes),
    Json(Box<JsonMatcherBody>),
    Text(Parameter)
}

impl From<&MultipartMatcherField> for ContentType {
    fn from(field: &MultipartMatcherField) -> Self {
        match field {
            MultipartMatcherField::Bytes(_) => ContentType::OctetStream,
            MultipartMatcherField::Json(_) => ContentType::ApplicationJson,
            MultipartMatcherField::Text(_) => ContentType::Text
        }
    }
}

impl MultipartMatcherField {
    async fn matches(&self, endpoint: &str, scope: &mut RequestScope<'_>, body: &mut IncomingMultipartBody) -> Validation {
        match self {
            Self::Bytes(bytes) => Ok(MatcherBody::match_bytes(bytes, body)),
            Self::Json(matcher) => matcher.matches(endpoint, scope, body).await,
            Self::Text(parameter) => {
                if let Some(input) = body.as_text() {
                    return parameter.matches(endpoint, input, scope).await;
                }

                Ok(false)
            }
        }
    }
}

#[derive(Clone, Debug, PartialEq, Eq)]
enum Parameter {
    AllOf(Vec<Parameter>),
    Any,
    AnyOf(Vec<Parameter>),
    Fixed(String),
    Integer,
    NotPresent,
    StartsWith(String),
    String,
    Uuid,
    Validated(String)
}

impl<'de> Deserialize<'de> for Parameter {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> {
        let value = Yml::deserialize(deserializer)?;
        Parameter::try_from(value).map_err(D::Error::custom)
    }
}

impl From<&str> for Parameter {
    fn from(data: &str) -> Self {
        Self::Fixed(data.to_string())
    }
}

impl From<String> for Parameter {
    fn from(data: String) -> Self {
        Parameter::from(data.as_str())
    }
}

impl Parameter {
    fn all(seq: Sequence) -> Anyhow<Self> {
        Ok(Self::AllOf(Self::parameters(seq)?))
    }

    #[async_recursion]
    async fn all_of(endpoint: &str, input: &str, scope: &RequestScope<'_>, params: &[Self]) -> Validation {
        for param in params {
            if ! param.matches(endpoint, input, scope).await? {
                return Ok(false);
            }
        }

        Ok(true)
    }

    fn any(seq: Sequence) -> Anyhow<Self> {
        Ok(Self::AnyOf(Self::parameters(seq)?))
    }

    #[async_recursion]
    async fn any_of(endpoint: &str, input: &str, scope: &RequestScope<'_>, params: &[Self]) -> Validation {
        for param in params {
            if param.matches(endpoint, input, scope).await? {
                return Ok(true);
            }
        }

        Ok(false)
    }

    fn is_not_present(&self) -> bool {
        match self {
            Self::AnyOf(ps) => ps.iter().any(Self::is_not_present),
            Self::NotPresent => true,
            _ => false
        }
    }

    async fn matches(&self, endpoint: &str, input: &str, scope: &RequestScope<'_>) -> Validation {
        match self {
            Self::Any => Ok(true),
            Self::Fixed(value) => Ok(value.eq(input)),
            Self::Integer => Ok(u64::from_str(input).is_ok()),
            Self::NotPresent => Ok(false),
            Self::Uuid => Ok(Uuid::parse_str(input).is_ok()),
            Self::StartsWith(value) => Ok(input.starts_with(value)),
            Self::String => Ok(!input.is_empty() && u64::from_str(input).is_err()),
            Self::AnyOf(params) => Self::any_of(endpoint, input, scope, params).await,
            Self::AllOf(params) => Self::all_of(endpoint, input, scope, params).await,
            Self::Validated(validator) => scope.validate(validator, endpoint, input).await
        }
    }

    fn parameters(sequence: Sequence) -> Anyhow<Vec<Self>> {
        let mut parameters = Vec::with_capacity(sequence.len());
        for value in sequence {
            let value = Parameter::try_from(value)?;

            parameters.push(value);
        }

        Ok(parameters)
    }

    fn typed(value: &str) -> Anyhow<Self> {
        match value {
            INTEGER => Ok(Self::Integer),
            STRING => Ok(Self::String),
            UUID => Ok(Self::Uuid),
            _ => Err(anyhow!("{}", INVALID_TYPE))
        }
    }
}

impl TryFrom<Mapping> for Parameter {
    type Error = AnyError;

    fn try_from(mapping: Mapping) -> Result<Self, Self::Error> {
        if mapping.len() == 1 {
            let value = Yml::from(mapping);
            if let Ok(parameter) = serde_yaml::from_value::<ParameterType>(value) {
                return Self::try_from(parameter);
            }
        }

        Err(anyhow!("{}", INVALID_MAPPING))
    }
}

impl TryFrom<Yml> for Parameter {
    type Error = AnyError;

    fn try_from(value: Yml) -> Result<Self, Self::Error> {
        match value {
            Yml::Null => Ok(Self::Fixed(EMPTY.to_string())),
            Yml::Bool(value) => Ok(Self::from(value.to_string())),
            Yml::Number(number) => Ok(Self::from(number.to_string())),
            Yml::String(data) => Ok(Self::from(data.as_str())),
            Yml::Mapping(value) => Self::try_from(value),
            Yml::Sequence(_) => Err(anyhow!("{}", PARAMS_NOT_FROM_SEQUENCES))
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "lowercase", deny_unknown_fields)]
enum ParameterType {
    #[serde(rename = "all_of")]
    AllOf(Sequence),
    Any(Option<String>),
    #[serde(rename = "any_of")]
    AnyOf(Sequence),
    #[serde(rename = "not_present")]
    NotPresent,
    Optional(Yml),
    #[serde(rename = "starts_with")]
    StartsWith(String),
    Validated(String)
}

impl TryFrom<ParameterType> for Parameter {
    type Error = AnyError;

    fn try_from(value: ParameterType) -> Result<Self, Self::Error> {
        match value {
            ParameterType::AllOf(seq) => Self::all(seq),
            ParameterType::Any(Some(value)) => Self::typed(&value),
            ParameterType::Any(_) => Ok(Self::Any),
            ParameterType::AnyOf(seq) => Self::any(seq),
            ParameterType::NotPresent => Ok(Self::NotPresent),
            ParameterType::StartsWith(value) => Ok(Self::StartsWith(value)),
            ParameterType::Validated(name) => Ok(Self::Validated(name)),
            ParameterType::Optional(yml) => {
                let parameter = Self::try_from(yml)?;
                Ok(Self::AnyOf(vec![Self::NotPresent, parameter]))
            }
        }
    }
}

#[derive(Clone, Debug, Default, Deserialize)]
struct Queries(HashMap<String, Parameter>);

impl Queries {
    async fn matches(&self, endpoint: &str, scope: &RequestScope<'_>) -> Validation {
        for (name, param) in &self.0 {
            let message = match scope.incoming.queries.get(name.as_str()) {
                Some(input) if ! param.matches(endpoint, input, scope).await? =>
                    format!("Query matching failed on {} for {:?} and input '{}'", name, param, input),
                None if ! param.is_not_present() => {
                    format!("Query matching failed on {} for {:?}. Parameter not present", name, param)
                },
                _ => continue
            };

            info!("{}", message);
            return Ok(false);
        }

        Ok(true)
    }
}

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "lowercase")]
enum RequestBody {
    #[default]
    Any,
    Bytes(FilePath),
    Empty,
    Form(JsonBody),
    Json(JsonBody),
    Multipart(MultipartBody)
}

impl RequestBody {
    #[async_recursion]
    async fn try_map(self, base: &Path) -> Anyhow<MatcherBody> {
        Ok(match self {
            Self::Any => MatcherBody::Any,
            Self::Empty => MatcherBody::Empty,
            Self::Bytes(filepath) => MatcherBody::Bytes(filepath.load_bytes(base).await?.into()),
            Self::Form(matcher) => MatcherBody::Form(matcher.try_map(base).await?),
            Self::Json(matcher) => MatcherBody::Json(matcher.try_map(base).await?),
            Self::Multipart(matcher) => MatcherBody::Multipart(matcher.try_map(base).await?)
        })
    }
}

#[derive(Debug, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "lowercase")]
enum JsonBody {
    Fixed(FilePath),
    Schema(FilePath),
    Validated(String)
}

impl JsonBody {
    async fn try_map(self, base: &Path) -> Anyhow<JsonMatcherBody> {
        Ok(match self {
            Self::Fixed(filepath) => JsonMatcherBody::Fixed(filepath.load_json(base).await?),
            Self::Validated(validator) => JsonMatcherBody::Validated(validator),
            Self::Schema(filepath) => {
                let schema = filepath.load_json(base).await?;
                JsonMatcherBody::Schema(JSchema::try_from(schema)?)
            }
        })
    }
}

#[derive(Debug, Default, Deserialize, PartialEq, Eq)]
struct MultipartBody(HashMap<String, Option<MultipartBodyField>>);

impl MultipartBody {
    #[async_recursion]
    async fn try_map(self, base: &Path) -> Anyhow<MultipartMatcherBody> {
        let inner = self.0;
        let capacity = inner.len();
        let mut multiparts = HashMap::with_capacity(capacity);
        for (key, value) in inner {
            let body = match value {
                Some(value) => Some(value.try_map(base).await?),
                None => None
            };

            multiparts.insert(key, body);
        }

        Ok(MultipartMatcherBody(multiparts))
    }
}

#[derive(Debug, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "lowercase")]
enum MultipartBodyField {
    Bytes(FilePath),
    Json(JsonBody),
    Text(Parameter),
}

impl MultipartBodyField {
    async fn try_map(self, base: &Path) -> Anyhow<MultipartMatcherField> {
        Ok(match self {
            Self::Bytes(filepath) =>
                MultipartMatcherField::Bytes(filepath.load_bytes(base).await?.into()),
            Self::Json(json) =>
                MultipartMatcherField::Json(Box::new(json.try_map(base).await?)),
            Self::Text(parameter) =>
                MultipartMatcherField::Text(parameter)
        })
    }
}

#[derive(Clone, Debug, Default, Deserialize)]
struct RequestHeaders(HashMap<HeaderKey, Parameter>);

impl RequestHeaders {
    async fn matches(&self, endpoint: &str, scope: &RequestScope<'_>) -> Validation {
        for (name, param) in &self.0 {
            let name = name.0.as_str();
            let message = match scope.incoming.headers.get(name) {
                Some(input) if ! param.matches(endpoint, input, scope).await? =>
                    format!("Header matching failed on {} for {:?} and input '{}'", name, param, input),
                None if ! param.is_not_present() => {
                    format!("Header matching failed on {} for {:?}. Parameter not present", name, param)
                },
                _ => continue
            };

            info!("{}", message);
            return Ok(false);
        }

        Ok(true)
    }
}

fn contains_wild_chars(segment: &str) -> bool {
    segment.contains(':') || segment.contains('*') || segment.contains('{') || segment.contains('}')
}

fn valid(path: &str) -> bool {
    let mut named = true;
    for (index, char) in path.chars().enumerate() {
        let colon = ':';
        if char.eq(&colon) {
            named = index != 0;
        }
    }

    named && ! path.is_empty()
}
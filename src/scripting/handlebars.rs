use handlebars::{Handlebars, handlebars_helper};

use crate::Registry;

pub fn handlebars() -> Registry {
    let mut registry = Handlebars::new();

    handlebars_helper!(city: |*_args| super::city());
    handlebars_helper!(email: |*_args| super::email());
    handlebars_helper!(firstname: |*_args| super::first_name());
    handlebars_helper!(paragraphs: |min: usize, max: usize| super::paragraphs(min, max));
    handlebars_helper!(sentences: |min: usize, max: usize| super::sentences(min, max));
    handlebars_helper!(state: |*_args| super::state());
    handlebars_helper!(time_rfc2822: |secs: i32| super::time_rfc2822(secs));
    handlebars_helper!(time_rfc3339: |secs: i32| super::time_rfc3339(secs));
    handlebars_helper!(timestamp: |*_args| super::timestamp());
    handlebars_helper!(uuid_v4: |*_args| super::uuid_v4());
    handlebars_helper!(word: |*_args| super::word());
    handlebars_helper!(words: |min: usize, max: usize| super::words(min, max));

    registry.register_helper("city", Box::new(city));
    registry.register_helper("email", Box::new(email));
    registry.register_helper("firstname", Box::new(firstname));
    registry.register_helper("paragraphs", Box::new(paragraphs));
    registry.register_helper("sentences", Box::new(sentences));
    registry.register_helper("state", Box::new(state));
    registry.register_helper("time_rfc2822", Box::new(time_rfc2822));
    registry.register_helper("time_rfc3339", Box::new(time_rfc3339));
    registry.register_helper("timestamp", Box::new(timestamp));
    registry.register_helper("uuid_v4", Box::new(uuid_v4));
    registry.register_helper("word", Box::new(word));
    registry.register_helper("words", Box::new(words));
    registry
}
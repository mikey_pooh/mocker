use std::collections::HashMap;
use std::fmt::Debug;

use anyhow::anyhow;
use boa_engine::{Context, JsResult as EngineResult, JsString, JsValue};
use boa_engine::object::{FunctionBuilder, JsObject, JsUint8Array};
use boa_engine::property::{Attribute, PropertyDescriptor};
use jsonwebtoken::{encode, EncodingKey, Header};
use serde_json::from_value;
use tokio::runtime::Runtime;
use tracing::{info, trace};
use tracing::log::{debug, error, warn};

use crate::{
    AFTER,
    AnyError,
    Anyhow,
    BEFORE,
    GENERATE,
    INTEGRITY_ERROR,
    Json,
    JsResult,
    JsThrown,
    scripting::{
        JsDetails,
        JsEvent,
        JsMessenger,
        JsParam,
        JsReply,
        ResponderError
    },
    ScriptLockType,
    State
};

const GENERATOR_CONVERSION_ERROR: &str = "Could not convert generator result to json or string.";
const INVALID_STATE_MUTATION: &str = "State is immutable. Expected [Dynamic, Persistent] as service state.";
const KNOWN_HANDLER_NOT_FUNCTION: &str = "Handler is not a runnable JS function.";
const KNOWN_HANDLER_UNAVAILABLE: &str = "Handlers should always be available. Please log an issue.";
const NO_MIN_OR_MAX: &str = "Generating paragraph requires a min and max number of paragraphs.";
const PARSE_ERROR: &str = "Fatal JS parse error. Terminating...";
const STATE_NOT_PRESENT: &str = "Backing state must always be present in this context. Please log an issue.";
const STATE_NOT_SERIALIZABLE: &str = "Backing state must always remain serializable. Invalid handler!";
const VALIDATOR_RETURNS_BOOL: &str = "Validators must return a boolean value.";

type BoaResult = EngineResult<JsValue>;

#[derive(Debug)]
pub(super) struct JsEventListener {
    messenger: JsMessenger,
    runner: JsRunner,
    runtime: Runtime
}

impl JsEventListener {
    pub(super) fn new(messenger: JsMessenger, runner: JsRunner, runtime: Runtime) -> Self {
        Self { messenger, runner, runtime }
    }

    pub(super) fn reply(&mut self, msg: JsResult) -> Anyhow<()> {
        self.messenger.reply(&self.runtime, msg)
    }

    pub(super) fn result(&mut self) -> Option<JsResult> {
        let scoped = self.messenger.receive(&self.runtime)?;
        scoped.span.in_scope(|| {
            Some(match scoped.event {
                JsEvent::After(details) => self.event(AFTER, details),
                JsEvent::Before(details) => self.event(BEFORE, details),
                JsEvent::Generate(details) => self.generate(GENERATE, details),
                JsEvent::Transform { name, details } => self.generate(&name, details),
                JsEvent::Validate { name,  details } => self.validate(&name, details),
            })
        })
    }

    fn args_with_state(&mut self, param: JsValue) -> Vec<JsValue> {
        let state = match self.runner.state {
            Some(ref state) => {
                let json = self.runtime.block_on(state.read());
                let context = &mut self.runner.context;
                JsValue::from_json(&json, context).expect(PARSE_ERROR)
            },
            None => JsValue::Null
        };

        vec![param, state]
    }

    fn call(&mut self, name: &str, endpoint: &str, args: &[JsValue]) -> BoaResult {
        let function = JsFunction { name, args };
        self.runner.run(endpoint, &function)
    }

    fn event(&mut self, name: &str, details: JsDetails) -> JsResult {
        let param = self.param(details.param);
        let mut args = self.args_with_state(param);
        match self.call(name, &details.endpoint, &args) {
            Ok(_) => {
                let value = args.remove(1);

                self.update_state(value);
                Ok(JsReply::Done)
            },
            Err(error) => Err(handle(error, &mut self.runner.context))
        }
    }

    fn generate(&mut self, name: &str, details: JsDetails) -> JsResult {
        let param = self.param(details.param);
        let args = self.args_with_state(param);
        match self.call(name, &details.endpoint, &args) {
            Ok(res) => {
                if let Ok(json) = res.to_json(&mut self.runner.context) {
                    return Ok(JsReply::Json(json));
                }

                Err(ResponderError::Internal(anyhow!(GENERATOR_CONVERSION_ERROR)))
            },
            Err(error) => Err(handle(error, &mut self.runner.context))
        }
    }

    fn param(&mut self, param: JsParam) -> JsValue {
        let context = &mut self.runner.context;
        match param {
            JsParam::None => JsValue::Null,
            JsParam::Json(json) => JsValue::from_json(&json, context).expect(PARSE_ERROR),
            JsParam::Parameter(data) => JsString::from(data).into(),
            JsParam::Bytes(bytes) => {
                let elements = bytes.into_iter();
                let array = JsUint8Array::from_iter(elements, context).expect(INTEGRITY_ERROR);

                array.into()
            }
        }

    }

    fn update_state(&mut self, value: JsValue) {
        let updated = value.to_json(&mut self.runner.context).expect(STATE_NOT_SERIALIZABLE);
        let run_state = self.runner.state.as_ref().expect(STATE_NOT_PRESENT);
        let mut state = self.runtime.block_on(run_state.write());

        *state = updated;
    }

    fn validate(&mut self, name: &str, details: JsDetails) -> JsResult {
        let args = [self.param(details.param)];
        match self.call(name, &details.endpoint, &args) {
            Ok(JsValue::Boolean(result)) => Ok(JsReply::Bool(result)),
            Ok(_) => Err(ResponderError::Internal(anyhow!(VALIDATOR_RETURNS_BOOL))),
            Err(error) => Err(handle(error, &mut self.runner.context))
        }
    }
}

#[derive(Debug)]
pub(super) struct JsRunner {
    context: Context,
    handlers: HashMap<String, JsValue>,
    state: Option<State>
}

impl JsRunner {
    pub(super) fn new(state: Option<State>) -> Anyhow<Self> {
        let handlers = HashMap::new();
        let context = Self::context()?;
        Ok(Self { context, handlers, state })
    }

    pub(super) fn add_handler(&mut self, name: &str, src: &str) -> Anyhow<ScriptLockType> {
        let result = self.context.eval(src).map_err(to_err)?;
        let object = result.to_object(&mut self.context).map_err(to_err)?;
        let lock = ScriptLockType::from_object(&object);
        if matches!(self.state, None) && lock.is_locking() {
            return Err(anyhow!("{}", INVALID_STATE_MUTATION));
        }

        self.handlers.insert(name.to_string(), result);
        Ok(lock)
    }

    fn run(&mut self, endpoint: &str, function: &JsFunction) -> BoaResult {
        let this = &self.handlers[endpoint];
        function.run(this, &mut self.context)
    }

    fn context() -> Anyhow<Context> {
        let mut context = Context::default();
        let console = console(&mut context).map_err(to_err)?;

        context.register_global_property("console", console, Attribute::default());
        context.register_global_function("city", 0, city);
        context.register_global_function("email", 0, email);
        context.register_global_function("firstName", 0, first_name);
        context.register_global_function("paragraphs", 0, paragraphs);
        context.register_global_function("sentences", 0, sentences);
        context.register_global_function("state", 0, state);
        context.register_global_function("time_rfc2822", 0, time_rfc2822);
        context.register_global_function("time_rfc3339", 0, time_rfc3339);
        context.register_global_function("word", 0, word);
        context.register_global_function("words", 0, words);
        context.register_global_function("uuidV4", 0, uuid_v4);

        Ok(context)
    }
}

#[derive(Debug)]
struct JsFunction<'e> {
    name: &'e str,
    args: &'e [JsValue]
}

impl JsFunction<'_> {
    fn run(&self, this: &JsValue, context: &mut Context) -> BoaResult {
        let handler = this.as_object().expect(INTEGRITY_ERROR);
        let value = handler.get(self.name, context).expect(KNOWN_HANDLER_UNAVAILABLE);
        let function = value.to_object(context).expect(KNOWN_HANDLER_NOT_FUNCTION);

        function.call(this, self.args, context)
    }
}

fn handle(error: JsValue, context: &mut Context) -> ResponderError {
    match error {
        error @ JsValue::Object(_) => {
            if let Ok(value) = error.to_json(context) {
                if let Ok(error) = from_value::<JsThrown>(value) {
                    return ResponderError::Thrown(error);
                }
            }

            ResponderError::Internal(to_err(error))
        },
        error => ResponderError::Internal(to_err(error))
    }
}

fn city(_: &JsValue, _: &[JsValue], _: &mut Context) -> BoaResult {
    let city: JsString = super::city().into();
    Ok(city.into())
}

fn console(context: &mut Context) -> EngineResult<JsObject> {
    let object = context.construct_object();
    let debug = FunctionBuilder::closure(context, debug_log).name("debug").build();
    let debug_desc = PropertyDescriptor::builder().value(debug).complete_with_defaults();
    let error = FunctionBuilder::closure(context, error_log).name("error").build();
    let error_desc = PropertyDescriptor::builder().value(error).complete_with_defaults();
    let info = FunctionBuilder::closure(context, info_log).name("info").build();
    let info_desc = PropertyDescriptor::builder().value(info).complete_with_defaults();
    let log = FunctionBuilder::closure(context, info_log).name("log").build();
    let log_desc = PropertyDescriptor::builder().value(log).complete_with_defaults();
    let warn = FunctionBuilder::closure(context, warn_log).name("warn").build();
    let warn_desc = PropertyDescriptor::builder().value(warn).complete_with_defaults();
    let trace = FunctionBuilder::closure(context, trace_log).name("trace").build();
    let trace_desc = PropertyDescriptor::builder().value(trace).complete_with_defaults();

    object.define_property_or_throw("debug", debug_desc, context)?;
    object.define_property_or_throw("error", error_desc, context)?;
    object.define_property_or_throw("info", info_desc, context)?;
    object.define_property_or_throw("log", log_desc, context)?;
    object.define_property_or_throw("warn", warn_desc, context)?;
    object.define_property_or_throw("trace", trace_desc, context)?;
    Ok(object)
}

fn debug_log(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    if let Some(data) = get_arg_as_str(args, 0) {
        debug!("{}", data);
    }

    Ok(JsValue::Undefined)
}

fn email(_: &JsValue, _: &[JsValue], _: &mut Context) -> BoaResult {
    let email: JsString = super::email().into();
    Ok(email.into())
}

fn error_log(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    if let Some(data) = get_arg_as_str(args, 0) {
        error!("{}", data);
    }

    Ok(JsValue::Undefined)
}

fn first_name(_: &JsValue, _: &[JsValue], _: &mut Context) -> BoaResult {
    let name: JsString = super::first_name().into();
    Ok(name.into())
}

fn get_arg_as_str(args: &[JsValue], index: usize) -> Option<&str> {
    Some(args.get(index)?.as_string()?.as_str())
}

fn get_arg_as_num(args: &[JsValue], index: usize) -> Option<f64> {
    args.get(index)?.as_number()
}

fn info_log(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    if let Some(data) = get_arg_as_str(args, 0) {
        info!("{}", data);
    }

    Ok(JsValue::Undefined)
}

#[allow(dead_code)]
fn jwt(claims: Json, secret: String) -> Anyhow<String> {
    let header = Header::default();
    let secret = secret.as_bytes();
    let key = EncodingKey::from_secret(secret);
    Ok(encode(&header, &claims, &key)?)
}

fn min_max_function<M>(args: &[JsValue], mapper: M) -> Option<JsString>
where M: Fn(usize, usize) -> JsString {
    let min = get_arg_as_num(args, 0)? as usize;
    let max = get_arg_as_num(args, 1)? as usize;
    Some(mapper(min, max))
}

fn paragraphs(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    let mapper = |min, max| super::paragraphs(min, max).into();
    match min_max_function(args, mapper) {
        Some(result) => Ok(result.into()),
        None => Err(JsString::from(NO_MIN_OR_MAX).into())
    }
}

fn sentences(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    let mapper = |min, max| super::sentences(min, max).into();
    match min_max_function(args, mapper) {
        Some(result) => Ok(result.into()),
        None => Err(JsString::from(NO_MIN_OR_MAX).into())
    }
}

fn state(_: &JsValue, _: &[JsValue], _: &mut Context) -> BoaResult {
    let state: JsString = super::state().into();
    Ok(state.into())
}

fn uuid_v4(_: &JsValue, _: &[JsValue], _: &mut Context) -> BoaResult {
    let uuid: JsString = super::uuid_v4().into();
    Ok(uuid.into())
}

fn to_err(error: JsValue) -> AnyError {
    anyhow!("{}", error.display())
}

fn time_rfc2822(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    let secs = get_arg_as_num(args, 0).map(|num| num as i32).unwrap_or(0);
    let time: JsString = super::time_rfc2822(secs).into();
    Ok(time.into())
}

fn time_rfc3339(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    let secs = get_arg_as_num(args, 0).map(|num| num as i32).unwrap_or(0);
    let time: JsString = super::time_rfc3339(secs).into();
    Ok(time.into())
}

fn trace_log(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    if let Some(data) = get_arg_as_str(args, 0) {
        trace!("{}", data);
    }

    Ok(JsValue::Undefined)
}

fn warn_log(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    if let Some(data) = get_arg_as_str(args, 0) {
        warn!("{}", data);
    }

    Ok(JsValue::Undefined)
}

fn word(_: &JsValue, _: &[JsValue], _: &mut Context) -> BoaResult {
    let word: JsString = super::word().into();
    Ok(word.into())
}

fn words(_: &JsValue, args: &[JsValue], _: &mut Context) -> BoaResult {
    let mapper = |min, max| super::words(min, max).into();
    match min_max_function(args, mapper) {
        Some(result) => Ok(result.into()),
        None => Err(JsString::from(NO_MIN_OR_MAX).into())
    }
}
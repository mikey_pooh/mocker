use anyhow::Result as Anyhow;
use structopt::StructOpt;
use tokio::task::spawn_blocking;

use mocker::args::Args;

#[tokio::main]
async fn main() ->  Anyhow<()> {
    let mut args: Args = Args::from_args();
    let (base, network) = spawn_blocking(move || args.compose()).await??;
    for server in network.start(base).await {
        if let Err(err) = server {
            return Err(err);
        }
    }

    Ok(())
}

use std::collections::{BTreeMap, HashMap};
use std::fmt::{Debug, Display, Formatter};
use std::fs::File as StdFile;
use std::io::{BufReader, SeekFrom};
use std::net::IpAddr;
use std::path::{Path, PathBuf};
use std::str::{from_utf8, FromStr};
use std::sync::Arc;

use actix_files::Files;
use actix_multipart::Multipart;
use actix_router::{Path as ActixPath, Router};
use actix_web::{App, FromRequest, HttpMessage, HttpRequest, HttpResponse, HttpResponseBuilder, HttpServer, ResponseError};
use actix_web::body::BoxBody;
use actix_web::dev::Payload;
use actix_web::http::header::{HeaderMap, HeaderName};
use actix_web::http::StatusCode;
use actix_web::web::{Bytes, Data};
use anyhow::anyhow;
use boa_engine::object::JsObject;
use derivative::Derivative;
use futures::future::join_all;
use futures::StreamExt;
use handlebars::{Handlebars, RenderError};
use reqwest::{Client as ReqwestClient, Method, Request as Reqwest, Response as Respwonse, Url};
use reqwest::header::HeaderValue;
use rustls::{Certificate, ConfigBuilder, PrivateKey, ServerConfig};
use rustls::server::WantsServerCert;
use rustls_pemfile::{certs, pkcs8_private_keys};
use serde::{Deserialize, Deserializer};
use serde::de::Error;
use serde_json::{from_str, from_slice, json, Map, to_vec, Value};
use serde_urlencoded::from_bytes;
use tokio::fs::{File, OpenOptions, read, read_to_string};
use tokio::io::{AsyncReadExt, AsyncSeekExt, AsyncWriteExt};
use tokio::sync::{Mutex, RwLock, Semaphore};
use tokio::sync::mpsc::channel;
use tracing::{debug, info};
use tracing_actix_web::TracingLogger;
use tracing_subscriber::filter::LevelFilter;

use crate::{
    endpoint::{
        Endpoint,
        EndpointDetails,
        Rule
    },
    logging::{
        start_logging,
        TracingSpan
    },
    responders::{
        MockerResponse,
        OutgoingBody
    },
    scripting::{
        Channels,
        handlebars::handlebars,
        JsDetails,
        JsEvent,
        JsEventQueue,
        JsMessenger,
        JsParam,
        JsReply,
        ScriptLockAction,
        ScriptLockGuard
    },
    service::factory
};
use crate::responders::Outgoing;

pub mod args;
mod endpoint;
mod logging;
mod matcher;
mod responders;
mod scripting;
mod service;

const AFTER: &str = "after";
const BEFORE: &str = "before";
const BODY: &str = "body";
const CONTENT_LENGTH: &str = "content-length";
const CONTENT_LENGTH_ERROR: &str = "Response content length cannot be overridden.";
const GENERATE: &str = "generate";
const HANDLER_BUILD_ERROR: &str = "Expected handler map unavailable. Please log an issue.";
const HEADERS: &str = "headers";
const INTEGRITY_ERROR: &str = "Application integrity failure. Please log an issue.";
const JSON: &str = "json";
const MAX_CONCURRENT_CLIENTS: usize = 25;
const MULTIPART_FORM_DATA: &str = "multipart/form-data";
const NO_CLIENT_PERMIT: &str = "Unable to acquire client permit. Please log an issue.";
const QUERIES: &str = "queries";
const REF_COUNT_ERROR: &str = "Reference count should be one at this point. Please log an issue.";
const SCRIPT_ERROR: &str = "Scripts returned unexpected type. Please log an issue.";
const SERIALISATION_ERROR: &str = "Could not serialize known serializable data. Please log an issue.";
const STATUS: &str = "status";

type AnyError = anyhow::Error;
type Anyhow<T> = anyhow::Result<T>;
type Generation = Result<MockerResponse, ResponderError>;
type GenerationBody = Result<OutgoingBody, ResponderError>;
type Handlers = HashMap<String, String>;
type Json = serde_json::Value;
type JsResult = Result<JsReply, ResponderError>;
type Registry = Handlebars<'static>;
type Response = Result<HttpResponse, ResponderError>;
type ScriptLockTypes = HashMap<String, ScriptLockType>;
type State = Arc<RwLock<Json>>;
type Validation = Result<bool, ResponderError>;
type Yml = serde_yaml::Value;

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Network {
    #[serde(default)]
    level: Level,
    services: HashMap<String, Service>
}

impl Network {
    pub async fn start(self, base: PathBuf) -> Vec<Anyhow<()>> {
        start_logging(self.level.0);

        let srvs = self.services.into_iter();
        let iter = srvs.map(|(n, s)| s.start(n, &base));
        join_all(iter).await
    }
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename_all = "lowercase")]
enum AppState {
    Dynamic(Option<FilePath>),
    #[default]
    None,
    Persistent(Option<PathBuf>),
    Static(FilePath)
}

impl AppState {
    async fn try_map(self, name: &str, base: &Path) -> Anyhow<ServiceState> {
        match self {
            Self::None => Ok(ServiceState::None),
            Self::Static(filepath) => Ok(ServiceState::Static(filepath.load_json(base).await?)),
            Self::Dynamic(Some(filepath)) => Self::dynamic(filepath.load_json(base).await?).await,
            Self::Dynamic(_) => Self::dynamic(json!({})).await,
            Self::Persistent(Some(relative)) => Self::persistent(base, &relative).await,
            Self::Persistent(_) => {
                let path = format!("{}.{}", name, JSON);
                let relative = Path::new(&path);
                Self::persistent(base, relative).await
            }
        }
    }

    async fn dynamic(json: Json) -> Anyhow<ServiceState> {
        let state = Arc::new(RwLock::new(json));
        Ok(ServiceState::Dynamic(state))
    }

    async fn open(path: &Path) -> Anyhow<File> {
        Ok(OpenOptions::new().read(true).write(true).create(true).open(path).await?)
    }

    async fn persistent(base: &Path, relative: &Path) -> Anyhow<ServiceState> {
        let path = resolve_relative_path(base, relative);
        let mut handle = Self::open(&path).await?;
        let input = read_file_to_string(&mut handle).await?;
        let json = Self::state(&input).await?;
        let state = Arc::new(RwLock::new(json));
        let handle = Mutex::new(handle);

        Ok(ServiceState::Persistent { handle, state })
    }

    async fn state(input: &str) -> Anyhow<Json> {
        if input.is_empty() { Ok(json!({})) } else { Ok(from_str(input)?) }
    }
}

#[derive(Clone, Debug, Deserialize, PartialEq, Eq)]
#[serde(deny_unknown_fields)]
struct Assets {
    folder: PathBuf,
    mount: String,
    traverse: bool
}

impl Assets {
    fn disallowed_path(&self) -> &str {
        &self.mount
    }

    fn to_files(&self, base: &Path) -> Files {
        let relative = &self.folder;
        let buffer = resolve_relative_path(base, relative);
        let mount_path = &self.mount;
        let folder = buffer.to_string_lossy();
        let serve_from = folder.as_ref();
        let mut files = Files::new(mount_path, serve_from);
        if self.traverse {
            files = files.show_files_listing();
        }

        files
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
struct HeaderKey(HeaderName);

impl<'de> Deserialize<'de> for HeaderKey {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> {
        let key = String::deserialize(deserializer)?;
        if ! key.eq_ignore_ascii_case(CONTENT_LENGTH) {
            let op = D::Error::custom;
            let name = HeaderName::from_str(&key).map_err(op)?;
            return Ok(HeaderKey(name));
        }

        Err(D::Error::custom(CONTENT_LENGTH_ERROR))
    }
}

#[derive(Debug)]
enum ContentType {
    ApplicationJson,
    Multipart(HashMap<String, ContentType>),
    OctetStream,
    Text,
    UrlEncoded
}

async fn resolve_json(body: &mut IncomingBody<'_>) -> Anyhow<()> {
    if let IncomingBody::Awaited(request, payload) = body {
        let bytes = take_body(request, payload).await?;
        let http_body = HttpBody::new(bytes);

        *body = IncomingBody::Json(http_body);
    }

    Ok(())
}

async fn resolve_multipart_form(body: &mut IncomingBody<'_>, types: &HashMap<String, ContentType>) -> Anyhow<()> {
    if let IncomingBody::Awaited(request, payload) = body {
        let content_type = request.content_type();
        if content_type.starts_with(MULTIPART_FORM_DATA) {
            let mut multipart = take_multipart(request, payload).await?;
            let mut parts = HashMap::new();
            while let Some(part) = multipart.next().await {
                let mut field = part?;
                let mut bytes = vec![];
                while let Some(chunk) = field.next().await {
                    bytes.extend(chunk?);
                }

                let name = field.name().to_string();
                let content_type = types.get(&name);
                let incoming = IncomingMultipartBody::with(bytes, content_type)?;

                parts.insert(name, incoming);
            }

            *body = IncomingBody::Multipart { body: None, parts };
            return Ok(());
        }

        let bytes = take_body(request, payload).await?;
        let http_body = HttpBody::new(bytes);
        *body = IncomingBody::Bytes(http_body);
    }

    Ok(())
}

async fn resolve_octet_stream(body: &mut IncomingBody<'_>) -> Anyhow<()> {
    if let IncomingBody::Awaited(request, payload) = body {
        let bytes = take_body(request, payload).await?;
        let http_body = HttpBody::new(bytes);

        *body = IncomingBody::Bytes(http_body);
    }

    Ok(())
}

async fn resolve_url_encoded(body: &mut IncomingBody<'_>) -> Anyhow<()> {
    if let IncomingBody::Awaited(request, payload) = body {
        let bytes = take_body(request, payload).await?;
        let http_body = HttpBody::new(bytes);

        *body = IncomingBody::UrlEncoded(http_body);
    }

    Ok(())
}

impl ContentType {
    async fn resolve(&self, body: &mut IncomingBody<'_>) -> Anyhow<()> {
        match self {
            Self::ApplicationJson => resolve_json(body).await,
            Self::Multipart(types) => resolve_multipart_form(body, types).await,
            Self::OctetStream => resolve_octet_stream(body).await,
            Self::UrlEncoded => resolve_url_encoded(body).await,
            // TODO: Add a text matcher here, just for completion and to remove the unreachable!
            _ => unreachable!()
        }
    }
}

#[derive(Debug)]
struct Incoming<'req> {
    path: &'req str,
    method: &'req Method,
    queries: HashMap<&'req str, &'req str>,
    headers: HashMap<&'req str, &'req str>
}

impl<'req> Incoming<'req> {
    fn new(request: &'req HttpRequest) -> Self {
        let path = request.path();
        let method = request.method();
        let queries = Self::queries(request.query_string());
        let headers = Self::headers(request.headers());

        Self { path, method, queries, headers }
    }

    fn headers(headers: &HeaderMap) -> HashMap<&str, &str> {
        let mut output = HashMap::new();
        for (key, value) in headers {
            if let Ok(value) = value.to_str() {
                output.insert(key.as_str(), value);
            }
        }

        output
    }

    fn queries(queries: &str) -> HashMap<&str, &str> {
        let mut output = HashMap::new();
        let queries = queries.split('&');
        for query in queries {
            let parts: Vec<_> = query.split('=').collect();
            if parts.len() == 2 {
                output.insert(parts[0], parts[1]);
            } else if parts.len() == 1 {
                output.insert(parts[0], "");
            }
        }

        output
    }
}

#[derive(Clone, Debug)]
struct HttpBody {
    bytes: Bytes,
    json: Option<Arc<Json>>,
    resolved: bool
}

impl HttpBody {
    fn new(bytes: Bytes) -> Self {
        Self { bytes, json: None, resolved: false }
    }

    fn with(bytes: Bytes, json: Arc<Json>) -> Self {
        Self { bytes, json: Some(json), resolved: true }
    }

    fn as_bytes(&self) -> Bytes {
        Bytes::clone(&self.bytes)
    }

    fn resolve_to_json(&mut self) -> Option<Arc<Json>> {
        if self.json.is_none() && ! self.resolved {
            self.resolved = true;

            let json = from_slice::<Json>(&self.bytes).ok()?;
            let json = Some(Arc::new(json));

            self.json = Option::clone(&json);
            return json;
        }

        Option::clone(&self.json)
    }

    fn resolve_to_json_encoded(&mut self) -> Option<Arc<Json>> {
        if self.json.is_none() && ! self.resolved {
            self.resolved = true;

            let json = from_bytes::<Json>(&self.bytes).ok()?;
            let json = Some(Arc::new(json));

            self.json = Option::clone(&json);
            return json;
        }

        Option::clone(&self.json)
    }

    fn take_json(&mut self) -> Option<Arc<Json>> {
        if self.json.is_none() && ! self.resolved {
            let json = from_slice::<Json>(&self.bytes).ok()?;
            return Some(Arc::new(json));
        }

        self.json.take()
    }

    fn take_json_encoded(&mut self) -> Option<Arc<Json>> {
        if self.json.is_none() && ! self.resolved {
            let json = from_bytes::<Json>(&self.bytes).ok()?;
            return Some(Arc::new(json));
        }

        self.json.take()
    }
}

enum IncomingBody<'req> {
    Awaited(&'req HttpRequest, Payload),
    Bytes(HttpBody),
    Empty,
    Json(HttpBody),
    Multipart {
        body: Option<HttpBody>,
        parts: HashMap<String, IncomingMultipartBody>
    },
    UrlEncoded(HttpBody)
}

impl Debug for IncomingBody<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Awaited(_, _) => write!(f, "Awaiting body."),
            Self::Empty => write!(f, "Empty body."),
            Self::Multipart { parts, .. } => write!(f, "{:?}", parts),
            Self::Bytes(body) |
            Self::Json(body) |
            Self::UrlEncoded(body) => write!(f, "{:?}", body),
        }
    }
}

trait Body {
    fn resolve_to_bytes(&mut self) -> Option<Bytes>;
    fn resolve_to_json(&mut self) -> Option<Arc<Json>>;
    fn resolve_to_param(&mut self) -> JsParam;
}

fn json_multiparts(parts: &HashMap<String, IncomingMultipartBody>) -> (Bytes, Arc<Json>) {
    let mut object = Map::new();
    for (key, body) in parts {
        if let Some(json) = body.as_json() {
            let name = key.to_string();
            object.insert(name, json);
        }
    }

    let json: Arc<Json> = Arc::new(object.into());
    let bytes: Bytes = to_vec(&json).expect(INTEGRITY_ERROR).into();

    (bytes, json)
}

impl Body for IncomingBody<'_> {
    fn resolve_to_bytes(&mut self) -> Option<Bytes> {
        match self {
            Self::Bytes(body) |
            Self::Json(body) |
            Self::UrlEncoded(body) => Some(body.as_bytes()),
            Self::Multipart { parts, body } => {
                if let Some(body) = body {
                    return Some(body.as_bytes());
                }

                let (bytes, json) = json_multiparts(parts);
                let http_body = HttpBody::with(Bytes::clone(&bytes), json);

                *body = Some(http_body);
                Some(bytes)
            },
            _ => None
        }
    }

    fn resolve_to_json(&mut self) -> Option<Arc<Json>> {
        match self {
            Self::Json(body) => body.resolve_to_json(),
            Self::UrlEncoded(body) => body.resolve_to_json_encoded(),
            Self::Multipart { parts, body } => {
                if let Some(body) = body {
                    return body.resolve_to_json();
                }

                let (bytes, json) = json_multiparts(parts);
                *body = Some(HttpBody::with(bytes, Arc::clone(&json)));
                Some(json)
            },
            _ => None
        }
    }

    fn resolve_to_param(&mut self) -> JsParam {
        match self {
            Self::Bytes(body) => body.as_bytes().into(),
            Self::Json(body) => {
                if let Some(json) = body.resolve_to_json() {
                    JsParam::Json(json)
                } else {
                    JsParam::None
                }
            },
            Self::UrlEncoded(body) => {
                if let Some(json) = body.resolve_to_json_encoded() {
                    JsParam::Json(json)
                } else {
                    JsParam::None
                }
            },
            Self::Multipart { parts, body } => {
                if let Some(body) = body {
                    let json = body.resolve_to_json().expect(INTEGRITY_ERROR);
                    return JsParam::Json(json);
                }

                let (bytes, json) = json_multiparts(parts);
                *body = Some(HttpBody::with(bytes, Arc::clone(&json)));
                JsParam::Json(json)
            },
            _ => JsParam::None
        }
    }
}

impl IncomingBody<'_> {
    async fn resolve(&mut self, content_type: Option<ContentType>) -> Anyhow<()> {
        match content_type {
            Some(content_type) => content_type.resolve(self).await,
            None => {
                if let Self::Awaited(request, payload) = self {
                    let bytes = take_body(request, payload).await?;
                    if ! bytes.is_empty() {
                        *self = Self::Bytes(HttpBody::new(bytes));
                        return Ok(());
                    }

                    *self = Self::Empty;
                }

                Ok(())
            }
        }
    }

    fn take(self) -> (Option<Bytes>, Option<Json>) {
        match self {
            Self::Bytes(body) => (Some(body.as_bytes()), None),
            Self::Multipart { parts, body } => {
                if let Some(mut body) = body {
                    let bytes = body.as_bytes();
                    let json = body.take_json().expect(INTEGRITY_ERROR);
                    let json = Arc::try_unwrap(json).expect(REF_COUNT_ERROR);
                    return (Some(bytes), Some(json));
                }

                let (bytes, json) = json_multiparts(&parts);
                let json = Arc::try_unwrap(json).expect(REF_COUNT_ERROR);
                (Some(bytes), Some(json))
            },
            Self::UrlEncoded(mut body) => {
                let bytes = body.as_bytes();
                if let Some(json) = body.take_json_encoded() {
                    let json = Arc::try_unwrap(json).expect(REF_COUNT_ERROR);
                    return (Some(bytes), Some(json));
                }

                (Some(bytes), None)
            }
            Self::Json(mut body) => {
                let bytes = body.as_bytes();
                if let Some(json) = body.take_json() {
                    let json = Arc::try_unwrap(json).expect(REF_COUNT_ERROR);
                    return (Some(bytes), Some(json));
                }

                (Some(bytes), None)
            },
            _ => (None, None)
        }
    }

    fn is_empty(&self) -> bool {
        matches!(self, Self::Empty)
    }
}

#[derive(Debug)]
enum IncomingMultipartBody {
    Bytes(Bytes),
    Json {
        bytes: Bytes,
        json: Arc<Json>
    },
    Text {
        bytes: Bytes,
        text: String
    }
}

impl Body for IncomingMultipartBody {
    fn resolve_to_bytes(&mut self) -> Option<Bytes> {
        match self {
            Self::Bytes(bytes) |
            Self::Json { bytes, .. } |
            Self::Text { bytes, .. } => Some(Bytes::clone(bytes))
        }
    }

    // TODO: Is it possible to remove the clone of the String here?
    fn resolve_to_json(&mut self) -> Option<Arc<Json>> {
        match self {
            Self::Json { json, .. } => Some(Arc::clone(json)),
            Self::Text { ref text, .. } => Some(Arc::new(Value::String(text.into()))),
            _ => None
        }
    }

    // TODO: Is it possible to remove the clone of the String here?
    fn resolve_to_param(&mut self) -> JsParam {
        match self {
            Self::Bytes(bytes) => JsParam::Bytes(Bytes::clone(bytes)),
            Self::Json { json, .. } => JsParam::Json(Arc::clone(json)),
            Self::Text { ref text, .. } => JsParam::Parameter(text.into())
        }
    }
}

impl IncomingMultipartBody {
    fn with(bytes: Vec<u8>, content_type: Option<&ContentType>) -> Anyhow<Self> {
        let bytes = Bytes::from(bytes);
        if let Some(content_type) = content_type {
            match content_type {
                ContentType::ApplicationJson => {
                    let json = Arc::new(from_slice::<Json>(&bytes)?);
                    return Ok(IncomingMultipartBody::Json { bytes, json });
                },
                ContentType::Text => {
                    let text = from_utf8(&bytes)?.to_string();
                    return Ok(IncomingMultipartBody::Text { text, bytes });
                },
                _ => {}
            }
        }

        Ok(IncomingMultipartBody::Bytes(bytes))
    }

    fn as_json(&self) -> Option<Json> {
        if let Self::Json { json, .. } = self {
            return Some(Json::clone(json));
        }

        None
    }

    fn as_text(&self) -> Option<&str> {
        if let Self::Text { text, .. } = self {
            return Some(text.as_str());
        }

        None
    }
}

#[derive(Debug, Deserialize)]
struct JsThrown {
    status: Status,
    message: Option<String>
}

impl Display for JsThrown {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.message {
            Some(ref message) => write!(f, "Exception thrown in JS. {}", message),
            None => write!(f, "Exception thrown in JS. No message")
        }
    }
}

impl From<&JsThrown> for HttpResponse {
    fn from(thrown: &JsThrown) -> Self {
        let mut builder = HttpResponseBuilder::new(thrown.status.0);
        match thrown.message {
            Some(ref message) => builder.body(message.to_string()),
            None => builder.finish()
        }
    }
}

#[derive(Copy, Clone, Debug)]
struct Level(LevelFilter);

impl<'de> Deserialize<'de> for Level {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> {
        let level = String::deserialize(deserializer)?;
        Ok(Self(LevelFilter::from_str(&level).map_err(D::Error::custom)?))
    }
}

impl Default for Level {
    fn default() -> Self {
        Self(LevelFilter::INFO)
    }
}

#[derive(Debug, Default, Deserialize)]
#[serde(rename_all = "lowercase", deny_unknown_fields)]
enum Protocol {
    #[default]
    Http,
    Https {
        key: PathBuf,
        cert: PathBuf
    }
}

impl Protocol {
    async fn serve(self, addr: (IpAddr, u16), base: &Path, assets: Option<Assets>, config: ServiceConfig) -> Anyhow<()> {
        let data = Data::new(config);
        let base_buf = base.to_path_buf();
        let info = Data::clone(&data);
        let server = HttpServer::new(move || {
            let ext = Data::clone(&data);
            let mw = TracingLogger::<TracingSpan>::new();
            let mut app = App::new();
            if let Some(assets) = &assets {
                let assets = assets.to_files(&base_buf);
                app = app.service(assets);
            }

            app.app_data(ext).service(factory).wrap(mw)
        });

        match self.cert_paths(base).await? {
            Some(config) => {
                let https_server = server.bind_rustls(addr, config)?;
                info!("Service '{}' bound at: https://{}:{}. Starting server", info.name, addr.0, addr.1);
                https_server.run().await?;
            },
            None => {
                let http_server = server.bind(addr)?;
                info!("Service '{}' bound at: http://{}:{}. Starting server", info.name, addr.0, addr.1);
                http_server.run().await?;
            }
        }

        Ok(())
    }

    async fn cert_paths(self, base: &Path) -> Anyhow<Option<ServerConfig>> {
        match self {
            Self::Https { key, cert } => {
                let key = &mut BufReader::new(std_file_at(base, &key).await?);
                let cert = &mut BufReader::new(std_file_at(base, &cert).await?);
                let chain = certs(cert)?.into_iter().map(Certificate).collect();
                let mut keys: Vec<PrivateKey> = pkcs8_private_keys(key)?.into_iter().map(PrivateKey).collect();
                let config = Self::base_config_builder();

                Ok(Some(config.with_single_cert(chain, keys.remove(0))?))
            },
            Self::Http => Ok(None)
        }
    }

    fn base_config_builder() -> ConfigBuilder<ServerConfig, WantsServerCert> {
        ServerConfig::builder().with_safe_defaults().with_no_client_auth()
    }
}

#[derive(Debug)]
struct RequestScope<'req> {
    channels: &'req Mutex<Channels>,
    incoming: Incoming<'req>
}

impl RequestScope<'_> {
    async fn validate<I>(&self, validator: &str, endpoint: &str, param: I) -> Validation
    where I: Into<JsParam> {
        let name = validator.to_string();
        let details = JsDetails::new(endpoint, param);
        let event = JsEvent::Validate { name, details };
        if let JsReply::Bool(result) = self.channels.lock().await.fire(event).await? {
            return Ok(result);
        }

        panic!("{}", SCRIPT_ERROR)
    }
}

#[derive(Debug)]
enum ResponderError {
    Internal(AnyError),
    Render(RenderError),
    Thrown(JsThrown)
}

impl Display for ResponderError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Internal(any) => write!(f, "{}", any),
            Self::Render(render) => write!(f, "{}", render),
            Self::Thrown(thrown) => write!(f, "{}", thrown)
        }
    }
}

impl From<AnyError> for ResponderError {
    fn from(err: AnyError) -> Self {
        Self::Internal(err)
    }
}

impl From<RenderError> for ResponderError {
    fn from(err: RenderError) -> Self {
        Self::Render(err)
    }
}

impl ResponseError for ResponderError {
    fn error_response(&self) -> HttpResponse<BoxBody> {
        match self {
            Self::Internal(_) | Self::Render(_) =>
                HttpResponse::new(StatusCode::INTERNAL_SERVER_ERROR),
            Self::Thrown(thrown) =>
                thrown.into()
        }
    }
}

#[derive(Copy, Clone, Debug, Default)]
enum ScriptLockType {
    After,
    Before,
    Both,
    #[default]
    Read
}

impl ScriptLockType {
    fn from_object(object: &JsObject) -> Self {
        let borrow = object.borrow();
        let mut lock = ScriptLockType::default();
        for key in borrow.properties().keys() {
            lock.add(key.into());
            if let ScriptLockType::Both = lock {
                break;
            }
        }

        lock
    }

    fn is_after(&self) -> bool {
        matches!(self, Self::After | Self::Both)
    }

    fn is_locking(&self) -> bool {
        matches!(self, Self::After | Self::Before | Self::Both)
    }
}

impl<T: ToString> From<T> for ScriptLockType {
    fn from(data: T) -> Self {
        let data = data.to_string();
        match data.as_str() {
            AFTER => ScriptLockType::After,
            BEFORE => ScriptLockType::Before,
            _ => ScriptLockType::Read
        }
    }
}

impl ScriptLockType {
    fn add(&mut self, lock: ScriptLockType) {
        match lock {
            Self::After => if let Self::Before = self {
                *self = Self::Both;
            } else if let Self::Read = self {
                *self = Self::After;
            },
            Self::Before => if let Self::After = self {
                *self = Self::Both;
            } else if let Self::Read = self {
                *self = Self::Before;
            },
            Self::Both => {
                *self = Self::Both;
            },
            _ => {}
        }
    }
}

#[derive(Clone, Debug)]
struct Client {
    client: ReqwestClient,
    semaphore: Arc<Semaphore>
}

impl Client {
    async fn send(&self, reqwest: Reqwest) -> Anyhow<Respwonse> {
        let _permit = self.semaphore.acquire().await.expect(NO_CLIENT_PERMIT);
        Ok(self.client.execute(reqwest).await?)
    }
}

impl Default for Client {
    fn default() -> Self {
        let client = ReqwestClient::new();
        let data = Semaphore::new(MAX_CONCURRENT_CLIENTS);
        let semaphore = Arc::new(data);

        Self { client, semaphore }
    }
}

#[derive(Clone, Debug)]
struct Request<'req> {
    path: &'req str,
    method: &'req Method,
    headers: HashMap<&'req str, &'req str>,
    queries: Option<String>,
    body: Option<Bytes>,
    json: Arc<Json>
}

impl<'req> Request<'req> {
    fn with(path: ActixPath<&'req str>, incoming: Incoming<'req>, body: IncomingBody<'req>) -> Self {
        let mut request = Self::map_from(&incoming);
        for (key, value) in path.iter() {
            let k = key.to_string();
            let v = Json::String(value.to_string());

            request.insert(k, v);
        }

        let (body, json) = body.take();
        if let Some(json) = json {
            request.insert(BODY.to_string(), json);
        }

        let json: Arc<Json> = Arc::new(request.into());
        let headers = incoming.headers;
        let queries = Self::queries(incoming.queries);
        Self { path: incoming.path, method: incoming.method, headers, queries, body, json }
    }

    fn reqwest(&self, base: &str) -> Anyhow<Reqwest> {
        let method = Method::clone(self.method);
        let url = self.url(base)?;
        let mut reqwest = Reqwest::new(method, url);
        let headers = reqwest.headers_mut();
        for (key, value) in &self.headers {
            let key = HeaderName::from_str(*key)?;
            let val = HeaderValue::from_str(*value)?;

            headers.insert(key, val);
        }

        let reqwest_body = reqwest.body_mut();
        if let Some(ref body) = self.body {
            let body = Bytes::clone(body).into();
            *reqwest_body = Some(body);
        }

        Ok(reqwest)
    }

    fn url(&self, base: &str) -> Anyhow<Url> {
        let url_string = if self.path.len() > 1 {
            format!("{}{}", base, &self.path[1..])
        } else {
            base.to_string()
        };

        let mut url = Url::from_str(&url_string)?;
        url.set_query(self.queries.as_deref());
        Ok(url)
    }

    fn map_from(incoming: &Incoming) -> Map<String, Json> {
        let mut request = Map::new();
        let queries = Self::object_from(incoming.queries.iter());
        let headers = Self::object_from(incoming.headers.iter());

        request.insert(QUERIES.to_string(), queries);
        request.insert(HEADERS.to_string(), headers);
        request
    }

    fn object_from<D>(iter: impl Iterator<Item=(D, D)>) -> Json
    where D: Display {
        let mut output = Map::new();
        for (key, value) in iter {
            let k = key.to_string();
            let v = Json::String(value.to_string());

            output.insert(k, v);
        }

        output.into()
    }

    fn queries(incoming: HashMap<&str, &str>) -> Option<String> {
        let length = incoming.len();
        if length == 0 { None } else {
            let mut queries = String::new();
            for (index, (key, value)) in incoming.into_iter().enumerate() {
                queries.push_str(key);
                queries.push('=');
                queries.push_str(value);

                if index < (length - 1) {
                    queries.push('&');
                }
            }

            Some(queries)
        }
    }
}

#[derive(Debug)]
struct ResponseScope<'res> {
    endpoint: &'res str,
    client: &'res Client,
    guard: ScriptLockGuard<'res>,
    registry: &'res Registry,
    request: Request<'res>,
    state: ResponseScopeState<'res>
}

impl<'res> ResponseScope<'res> {
    fn with(endpoint: &'res str, request: Request<'res>, config: &'res ServiceConfig) -> Self {
        let guard = ScriptLockGuard::Deferred(&config.channels);
        let registry = &config.registry;
        let state = ResponseScopeState::from(&config.state);
        let client = &config.client;

        Self { endpoint, client, guard, registry, request, state }
    }

    async fn after(&mut self, param: Arc<Json>) -> JsResult {
        let details = JsDetails::new(self.endpoint, param);
        let action = ScriptLockAction::Release;
        let event = JsEvent::After(details);
        return self.guard.reply(event, action).await;
    }

    async fn before(&mut self, lock: ScriptLockType) -> JsResult {
        let param = Arc::clone(&self.request.json);
        let details = JsDetails::new(self.endpoint, param);
        let action = if matches!(lock, ScriptLockType::Before | ScriptLockType::Both) {
            ScriptLockAction::Upgrade
        } else {
            ScriptLockAction::None
        };

        let event = JsEvent::Before(details);
        return self.guard.reply(event, action).await;
    }

    async fn generate(&mut self, param: Arc<Json>, lock: ScriptLockType) -> JsResult {
        let details = JsDetails::new(self.endpoint, param);
        let event = JsEvent::Generate(details);
        let action = if matches!(lock, ScriptLockType::Before) {
            ScriptLockAction::Release
        } else {
            ScriptLockAction::None
        };

        self.guard.reply(event, action).await
    }

    async fn transform(&mut self, name: &str, param: Arc<Json>, lock: ScriptLockType) -> JsResult {
        let details = JsDetails::new(self.endpoint, param);
        let name = name.to_string();
        let event = JsEvent::Transform { name, details };
        let action = if matches!(lock, ScriptLockType::Before) {
            ScriptLockAction::Release
        } else {
            ScriptLockAction::None
        };

        self.guard.reply(event, action).await
    }
}

#[derive(Debug)]
enum ResponseScopeState<'res> {
    None,
    Open(&'res Json),
    Protected(&'res State)
}

impl ResponseScopeState<'_> {
    async fn bytes(&self) -> Option<Vec<u8>> {
        match *self {
            Self::None => None,
            Self::Open(value) => Some(to_vec(value).expect(SERIALISATION_ERROR)),
            Self::Protected(locked) => {
                let value = locked.read().await;
                Some(to_vec(&*value).expect(SERIALISATION_ERROR))
            }
        }
    }
}

fn status(map: &mut Map<String, Value>) -> Result<StatusCode, ResponderError> {
    let err = || anyhow!("Invalid status from transformer");
    let status = map.remove(STATUS).ok_or_else(err)?.as_u64().ok_or_else(err)?;
    let src = u16::try_from(status).map_err(|_| err())?;

    Ok(StatusCode::from_u16(src).map_err(|_| err())?)
}

fn headers(map: &mut Map<String, Value>) -> Result<HeaderMap, ResponderError> {
    let err = || anyhow!("Invalid headers from transformer");
    let value = map.remove(HEADERS).ok_or_else(err)?;
    let header_map = value.as_object().ok_or_else(err)?;
    let mut headers = HeaderMap::with_capacity(header_map.len());
    for (key, value) in header_map {
        let value = value.as_str().ok_or_else(err)?;
        let key = HeaderName::from_str(key.as_str()).map_err(|_| err())?;
        let val = HeaderValue::from_str(value).map_err(|_| err())?;

        headers.insert(key, val);
    }

    Ok(headers)
}

fn body(map: &mut Map<String, Value>) -> Result<OutgoingBody, ResponderError> {
    let err = |_| anyhow!("Invalid body from transformer.");
    if let Some(body) = map.remove(BODY) {
        let bytes = to_vec(&body).map_err(err)?;
        return Ok(OutgoingBody::Bytes(bytes.into()));
    }

    Ok(OutgoingBody::Empty)
}

#[derive(Debug)]
struct Transformed(Arc<Json>);

impl From<Transformed> for Response {
    fn from(transformed: Transformed) -> Self {
        let this = transformed.0;
        let mut json = Arc::try_unwrap(this).expect(INTEGRITY_ERROR);
        let map = json.as_object_mut().expect(INTEGRITY_ERROR);
        let status = status(map)?;
        let headers = headers(map)?;
        let body = body(map)?;
        let outgoing = Outgoing::new(status, headers, body);

        Ok(outgoing.into())
    }
}

#[derive(Debug)]
struct Route(Vec<Endpoint>);

impl Display for Route {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let mut output = String::from("[");
        let length = self.0.len();
        for (index, endpoint) in self.0.iter().enumerate() {
            output.push_str(endpoint.name());
            if index != length - 1 {
                output.push_str(", ");
            }
        }

        output.push(']');
        write!(f, "{}", output)
    }
}

impl Route {
    fn new(endpoint: Endpoint) -> Self {
        Self(vec![endpoint])
    }

    fn add(&mut self, endpoint: Endpoint) {
        self.0.push(endpoint);
        self.0.sort_unstable();
    }

    fn endpoints(&self) -> &[Endpoint] {
        self.0.as_slice()
    }

    fn set_locks(&mut self, locks: &ScriptLockTypes) {
        for endpoint in &mut self.0 {
            endpoint.set_lock(locks);
        }
    }
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
struct Service {
    host: IpAddr,
    port: u16,
    #[serde(default)]
    protocol: Protocol,
    assets: Option<Assets>,
    rules: HashMap<String, Rule>,
    #[serde(default)]
    state: AppState
}

impl Service {
    async fn start(self, name: String, base: &Path) -> Anyhow<()> {
        let (event_sender, event_receiver) = channel(1);
        let (result_sender, result_receiver) = channel(1);
        let channels = Channels::new(event_sender, result_receiver);
        let registry = handlebars();
        let service_state = self.state.try_map(&name, base).await?;
        let state = service_state.as_state();
        let mut builder = ServiceBuilder::new(channels, registry, service_state);
        let disallowed = self.assets.as_ref().map(Assets::disallowed_path);
        for (name, rule) in self.rules {
            rule.populate_builder(name, disallowed, base, &mut builder).await?;
        }

        let handlers = builder.take_handlers().expect(HANDLER_BUILD_ERROR);
        let messenger = JsMessenger::new(event_receiver, result_sender);
        let queue = JsEventQueue::new(handlers, messenger, state);
        let locks = queue.start().await?;
        let config = builder.finish(name, locks);
        let addr = (self.host, self.port);
        self.protocol.serve(addr, base, self.assets, config).await
    }
}

#[derive(Debug)]
struct ServiceBuilder {
    channels: Channels,
    handlers: Option<Handlers>,
    registry: Registry,
    routes: BTreeMap<EndpointDetails, Route>,
    state: ServiceState
}

impl ServiceBuilder {
    fn new(channels: Channels, registry: Registry, state: ServiceState) -> Self {
        let handlers = Some(HashMap::default());
        let routes = BTreeMap::default();
        Self { channels, handlers, registry, routes, state }
    }

    fn add_endpoint(&mut self, details: EndpointDetails, endpoint: Endpoint) {
        if let Some(route) = self.routes.get_mut(&details) {
            return route.add(endpoint);
        }

        let route = Route::new(endpoint);
        self.routes.insert(details, route);
    }

    fn add_script_for<I>(&mut self, name: I, source: String)
    where I: Into<String> {
        let scripts = self.handlers.as_mut().expect(HANDLER_BUILD_ERROR);
        scripts.entry(name.into()).or_insert(source);
    }

    fn finish(self, name: String, locks: ScriptLockTypes) -> ServiceConfig {
        let mut builder = Router::build();
        for (details, mut route) in self.routes {
            let path = details.match_path();
            info!("Adding endpoint(s) '{}' to '{}' at '{}'", route, name, path);

            debug!("Script Lock Types are: {:?}", locks);
            route.set_locks(&locks);
            builder.path(path, route);
        }

        let registry = self.registry;
        let state = self.state;
        let channels = Mutex::new(self.channels);
        let router = builder.finish();
        let client = Client::default();
        ServiceConfig { name, client, channels, registry, router, state }
    }

    fn take_handlers(&mut self) -> Option<Handlers> {
        self.handlers.take()
    }
}

#[derive(Derivative)]
#[derivative(Debug)]
struct ServiceConfig {
    name: String,
    channels: Mutex<Channels>,
    client: Client,
    registry: Registry,
    #[derivative(Debug="ignore")]
    router: Router<Route>,
    state: ServiceState
}

impl ServiceConfig {
    fn match_at<'p>(&'p self, path: &'p str) -> Option<(ActixPath<&'p str>, &Route)> {
        let mut path = ActixPath::new(path);
        if let Some((route, _)) = self.router.recognize(&mut path) {
            return Some((path, route));
        }

        None
    }
}

#[derive(Debug)]
enum ServiceState {
    Dynamic(State),
    None,
    Persistent {
        handle: Mutex<File>,
        state: State
    },
    Static(Json)
}

impl<'service> From<&'service ServiceState> for ResponseScopeState<'service> {
    fn from(state: &'service ServiceState) -> Self {
        match state {
            ServiceState::Persistent { state, .. } | ServiceState::Dynamic(state) =>
                Self::Protected(state),
            ServiceState::Static(json) =>
                Self::Open(json),
            ServiceState::None =>
                Self::None
        }
    }
}

impl ServiceState {
    fn as_state(&self) -> Option<State> {
        match self {
            Self::Dynamic(state) | Self::Persistent { state, .. } => Some(Arc::clone(state)),
            Self::Static(_) | Self::None => None
        }
    }

    async fn persist(&self, persistent: bool) -> Anyhow<()> {
        if persistent {
            if let Self::Persistent { handle, state } = self {
                let json = state.read().await;
                let bytes = to_vec(&*json)?;
                let mut handle = handle.lock().await;

                handle.set_len(0).await?;
                handle.seek(SeekFrom::Start(0)).await?;
                handle.write_all(&bytes).await?;
                handle.flush().await?;
            }
        }

        Ok(())
    }
}

#[derive(Copy, Clone, Debug, Default, PartialEq, Eq)]
struct Status(StatusCode);

impl<'de> Deserialize<'de> for Status {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where D: Deserializer<'de> {
        let value = u16::deserialize(deserializer)?;
        let code = StatusCode::try_from(value).map_err(D::Error::custom)?;

        Ok(Self(code))
    }
}

#[derive(Debug, Deserialize, PartialEq, Eq)]
struct FilePath(PathBuf);

impl FilePath {
    async fn load_bytes(self, base: &Path) -> Anyhow<Vec<u8>> {
        let relative = self.0;
        let path = resolve_relative_path(base, &relative);
        Ok(read(path).await?)
    }

    async fn load_json(self, base: &Path) -> Anyhow<Json> {
        let data = self.load_string(base).await?;
        Ok(from_str::<Json>(&data)?)
    }

    async fn load_string(self, base: &Path) -> Anyhow<String> {
        let relative = self.0;
        let path = resolve_relative_path(base, &relative);
        Ok(read_to_string(path).await?)
    }
}

async fn std_file_at(base: &Path, relative: &Path) -> Anyhow<StdFile> {
    let resolved = resolve_relative_path(base, relative);
    Ok(File::open(resolved).await?.into_std().await)
}

async fn read_file_to_string(handle: &mut File) -> Anyhow<String> {
    let mut data = String::new();

    handle.read_to_string(&mut data).await?;
    Ok(data)
}

fn resolve_relative_path(base: &Path, relative: &Path) -> PathBuf {
    let mut path = base.to_path_buf();

    path.push(relative);
    path
}

async fn take_multipart(request: &HttpRequest, payload: &mut Payload) -> Anyhow<Multipart> {
    Multipart::from_request(request, payload).await.map_err(|err| anyhow!("{}", err))
}

async fn take_body(request: &HttpRequest, payload: &mut Payload) -> Anyhow<Bytes> {
    Bytes::from_request(request, payload).await.map_err(|err| anyhow!("{}", err))
}

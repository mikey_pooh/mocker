use std::cmp::Ordering;
use std::hash::Hash;
use std::path::{Path, PathBuf};
use std::sync::Arc;
use std::time::Duration;

use actix_web::http::Method;
use serde::Deserialize;
use serde_json::Map;
use tokio::fs::read_to_string;
use tokio::time::sleep;
use tracing::info;

use crate::{
    Anyhow,
    IncomingBody,
    INTEGRITY_ERROR,
    Json,
    matcher::{
        Matcher,
        RequestMatcher
    },
    MockerResponse,
    Request,
    RequestScope,
    resolve_relative_path,
    responders::{
        Responder,
        ResponderBuilder
    },
    Response,
    ResponseScope,
    ScriptLockType,
    ScriptLockTypes,
    ServiceBuilder,
    ServiceConfig,
    Validation,
    Transformed
};

#[derive(Debug)]
pub(super) struct Endpoint {
    name: String,
    matcher: Matcher,
    lock: ScriptLockType,
    responder: Responder,
    priority: usize,
    delay: Option<u64>
}

impl Endpoint {
    pub(super) async fn delay(&self) {
        if let Some(duration) = self.delay.map(Duration::from_millis) {
            sleep(duration).await;
        }
    }

    pub(super) async fn matches(&self, scope: &mut RequestScope<'_>, body: &mut IncomingBody<'_>) -> Validation {
        self.matcher.matches(&self.name, scope, body).await
    }

    pub(super) fn name(&self) -> &str {
        &self.name
    }

    pub(super) fn persistent(&self) -> bool {
        self.lock.is_locking()
    }

    pub(super) async fn response(&self, request: Request<'_>, config: &ServiceConfig) -> Response {
        let mut scope = ResponseScope::with(&self.name, request, config);
        if matches!(self.lock, ScriptLockType::Before | ScriptLockType::Both) {
            info!("Running before function for {}", self.name);
            scope.before(self.lock).await?;
        }

        let mut response = self.responder.respond(self.lock, &mut scope).await?;
        let transformer = self.responder.transformer();
        if transformer.is_some() || self.lock.is_after() {
            let outgoing = response.as_outgoing_mut().expect(INTEGRITY_ERROR);
            let mut param = Arc::new(Json::from(outgoing));
            if let Some(name) = transformer {
                info!("Running transformation function for {}", self.name);
                let parameter = Arc::clone(&param);
                let result = scope.transform(name, parameter, self.lock).await?;
                let output = Map::try_from(result)?;

                param = Arc::new(output.into());
                response = MockerResponse::Transformed(Transformed(Arc::clone(&param)));
            }

            if self.lock.is_after() {
                info!("Running after function for {}", self.name);
                scope.after(param).await?;
            }
        }

        response.into()
    }

    pub(super) fn set_lock(&mut self, locks: &ScriptLockTypes) {
        if let Some(lock) = locks.get(&self.name) {
            self.lock = *lock;
        }
    }
}

impl Eq for Endpoint { }

impl PartialEq for Endpoint {
    fn eq(&self, other: &Self) -> bool {
        self.priority.eq(&other.priority)
    }
}

impl PartialOrd for Endpoint {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Endpoint {
    fn cmp(&self, other: &Self) -> Ordering {
        self.priority.cmp(&other.priority).reverse()
    }
}

#[derive(Debug, Hash, PartialEq, Eq)]
pub(super) struct EndpointDetails {
    path: String,
    method: Method
}

impl EndpointDetails {
    pub(super) fn new(path: String, method: Method) -> Self {
        Self { path, method }
    }

    pub(super) fn match_path(self) -> String {
        format!("{}{}", self.method, self.path.as_str())
    }
}

impl Ord for EndpointDetails {
    fn cmp(&self, other: &Self) -> Ordering {
        if self != other {
            let this = concrete_segments(&self.path);
            let others = concrete_segments(&other.path);
            let ordering = this.cmp(&others);
            if let Ordering::Equal = ordering {
                return match self.path.cmp(&other.path) {
                    Ordering::Equal => {
                        let method = self.method.as_str();
                        let other = other.method.as_str();
                        method.cmp(other)
                    },
                    ordering => ordering
                }
            }

            return ordering.reverse();
        }

        Ordering::Equal
    }
}

impl PartialOrd for EndpointDetails {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, Deserialize)]
#[serde(deny_unknown_fields)]
pub(super) struct Rule {
    #[serde(default)]
    #[serde(rename = "when")]
    request: RequestMatcher,
    #[serde(default)]
    #[serde(rename = "then")]
    responder: ResponderBuilder,
    script: Option<PathBuf>,
    delay: Option<u64>,
    #[serde(default)]
    priority: usize
}

impl Rule {
    pub(super) async fn populate_builder(self, name: String, disallowed: Option<&str>, base: &Path, builder: &mut ServiceBuilder) -> Anyhow<()> {
        let matchers = self.request.try_map(disallowed, base).await?;
        let responder = self.responder.try_map(&name, base, &mut builder.registry).await?;
        // TODO: For the most part, there will only be one matcher, making many of these clones unnecessary in the general case
        // TODO: Use a trait to abstract away this unnecessary cloning where possible
        for (details, matcher) in matchers {
            let name = String::clone(&name);
            let responder = Responder::clone(&responder);
            let lock = ScriptLockType::default();
            let delay = self.delay;
            let priority = self.priority;
            let endpoint = Endpoint { name, matcher, lock, responder, priority, delay };
            let script = Option::clone(&self.script);
            if let Some(source) = script {
                let path = resolve_relative_path(base, &source);
                let source = read_to_string(path).await?;

                builder.add_script_for(&endpoint.name, source);
            }

            builder.add_endpoint(details, endpoint);
        }

        Ok(())
    }
}

fn concrete_segments(input: &str) -> i32 {
    let segments = input.split('/');
    let mut count = 0;
    for segment in segments {
        if segment.contains('{') && segment.contains('}') {
            continue;
        }

        count += 1;
    }

    count
}
use std::collections::HashMap;
use std::sync::Arc;
use std::thread;
use std::time::{SystemTime, UNIX_EPOCH};

use actix_web::web::Bytes;
use anyhow::anyhow;
use chrono::{Datelike, DateTime, FixedOffset, Timelike, TimeZone, Utc};
use fake::Fake;
use fake::faker::address::en::{CityName, StateName};
use fake::faker::internet::en::SafeEmail;
use fake::faker::lorem::en::{Paragraph, Sentence, Word, Words};
use fake::faker::name::en::FirstName;
use serde_json::Map;
use tokio::runtime::{Builder, Runtime};
use tokio::sync::{Mutex, MutexGuard};
use tokio::sync::mpsc::{Receiver, Sender};
use tokio::sync::oneshot::channel;
use tracing::{debug, Span, warn};
use uuid::Uuid;

use crate::{AnyError, Anyhow, Json, JsResult, ResponderError, scripting::javascript::{
    JsEventListener,
    JsRunner
}, ScriptLockTypes, State};

pub mod handlebars;
mod javascript;

const COMMS_ERROR: &str = "Fatal channel communications error.";
const INVALID_OFFSET: &str = "Offset ignored (using UTC). Must be between -86399 and +86399 (inclusive).";
const INVALID_PASSTHROUGH_TRANSFORM: &str = "Passthrough transformation functions must return a json map containing a status, headers and body.";

#[derive(Debug)]
pub(super) struct Channels {
    receiver: Receiver<JsResult>,
    sender: Sender<JsScopedEvent>
}

impl Channels {
    pub(super) fn new(sender: Sender<JsScopedEvent>, receiver: Receiver<JsResult>) -> Self {
        Self { receiver, sender }
    }

    pub(super) async fn fire(&mut self, event: JsEvent) -> JsResult {
        let span = Span::current();
        let scoped = JsScopedEvent { span, event };

        self.sender.send(scoped).await.expect(COMMS_ERROR);
        self.receiver.recv().await.expect(COMMS_ERROR)
    }
}

#[derive(Debug)]
pub(super) struct JsEventQueue {
    handlers: HashMap<String, String>,
    messenger: JsMessenger,
    state: Option<State>
}

impl JsEventQueue {
    pub(super) fn new(handlers: HashMap<String, String>, messenger: JsMessenger, state: Option<State>) -> Self {
        Self { handlers, messenger, state }
    }

    pub(super) async fn start(self) -> Anyhow<ScriptLockTypes> {
        let runtime = Builder::new_current_thread().enable_all().build()?;
        let (sender, receiver) = channel();
        thread::spawn(move || {
            let capacity = self.handlers.len();
            let mut locks = ScriptLockTypes::with_capacity(capacity);
            let runner = match JsRunner::new(self.state) {
                Err(err) => return sender.send(Err(err)).expect(COMMS_ERROR),
                Ok(mut runner) => {
                    for (name, src) in self.handlers {
                        let lock = runner.add_handler(&name, &src);
                        if let Err(err) = lock {
                            return sender.send(Err(err)).expect(COMMS_ERROR);
                        }

                        locks.insert(name, lock.unwrap());
                    }

                    runner
                }
            };

            sender.send(Ok(locks)).expect(COMMS_ERROR);
            start(self.messenger, runner, runtime);
        });

        receiver.await?
    }
}

#[derive(Debug)]
pub(super) struct JsDetails {
    endpoint: String,
    param: JsParam
}

impl JsDetails {
    pub(super) fn new<P, S>(endpoint: S, param: P) -> Self
        where P: Into<JsParam>, S: Into<String> {
        let endpoint = endpoint.into();
        let param = param.into();

        Self { endpoint, param }
    }
}

#[derive(Debug)]
pub(super) struct JsScopedEvent {
    span: Span,
    event: JsEvent
}

#[derive(Debug)]
pub(super) enum JsEvent {
    After(JsDetails),
    Before(JsDetails),
    Generate(JsDetails),
    Transform {
        name: String,
        details: JsDetails
    },
    Validate {
        name: String,
        details: JsDetails
    }
}

#[derive(Debug)]
pub(super) struct JsMessenger {
    receiver: Receiver<JsScopedEvent>,
    sender: Sender<JsResult>
}

impl JsMessenger {
    pub(super) fn new(receiver: Receiver<JsScopedEvent>, sender: Sender<JsResult>) -> Self {
        Self { receiver, sender }
    }

    fn receive(&mut self, runtime: &Runtime) -> Option<JsScopedEvent> {
        let receive = self.receiver.recv();
        runtime.block_on(receive)
    }

    fn reply(&self, runtime: &Runtime, msg: JsResult) -> Anyhow<()> {
        let send = self.sender.send(msg);
        Ok(runtime.block_on(send)?)
    }
}

// TODO: Refactor so this can be used across ALL JS functions
#[derive(Debug)]
pub(super) enum JsParam {
    Bytes(Bytes),
    Json(Arc<Json>),
    None,
    Parameter(String)
}

impl From<&str> for JsParam {
    fn from(value: &str) -> Self {
        Self::Parameter(value.to_string())
    }
}

impl From<Arc<Json>> for JsParam {
    fn from(json: Arc<Json>) -> Self {
        Self::Json(json)
    }
}

impl From<Bytes> for JsParam {
    fn from(bytes: Bytes) -> Self {
        Self::Bytes(bytes)
    }
}

#[derive(Debug)]
pub(super) enum JsReply {
    Bool(bool),
    Done,
    Json(Json)
}

impl TryFrom<JsReply> for Map<String, Json> {
    type Error = AnyError;

    fn try_from(reply: JsReply) -> Result<Self, Self::Error> {
        if let JsReply::Json(Json::Object(map)) = reply {
            return Ok(map);
        }

        Err(anyhow!(INVALID_PASSTHROUGH_TRANSFORM))
    }
}

#[derive(Debug)]
pub(super) enum ScriptLockAction {
    None,
    Release,
    Upgrade
}

#[derive(Debug)]
pub(super) enum ScriptLockGuard<'res> {
    Deferred(&'res Mutex<Channels>),
    Locked {
        channels: MutexGuard<'res, Channels>,
        lock: &'res Mutex<Channels>
    }
}

impl ScriptLockGuard<'_> {
    pub(super) async fn reply(&mut self, event: JsEvent, action: ScriptLockAction) -> JsResult {
        match *self {
            Self::Locked { ref mut channels, lock } => {
                let result = channels.fire(event).await;
                if let ScriptLockAction::Release = action {
                    debug!("Channel lock released");
                    *self = Self::Deferred(lock);
                }

                result
            },
            Self::Deferred(lock) => {
                let mut channels = lock.lock().await;
                let result = channels.fire(event).await;
                if let ScriptLockAction::Upgrade = action {
                    debug!("Channel lock held");
                    *self = Self::Locked { channels, lock };
                }

                result
            }
        }
    }
}

fn start(messenger: JsMessenger, runner: JsRunner, runtime: Runtime) {
    let mut listener = JsEventListener::new(messenger, runner, runtime);
    loop {
        if let Some(msg) = listener.result() {
            if listener.reply(msg).is_ok() {
                continue;
            }
        }
    }
}

fn city() -> String {
    CityName().fake()
}

fn email() -> String {
    SafeEmail().fake()
}

fn first_name() -> String {
    FirstName().fake()
}

fn offset(secs: i32) -> FixedOffset {
    FixedOffset::east_opt(secs).unwrap_or_else(|| {
        warn!("{}", INVALID_OFFSET);
        FixedOffset::east(0)
    })
}

fn paragraphs(min: usize, max: usize) -> String {
    let count = min..(max + 1);
    Paragraph(count).fake()
}

fn sentences(min: usize, max: usize) -> String {
    let count = min..(max + 1);
    Sentence(count).fake()
}

fn state() -> String {
    StateName().fake()
}

fn time(secs: i32) -> DateTime<FixedOffset> {
    let date = Utc::now();
    let year = date.year();
    let month = date.month();
    let day = date.day();
    let hour = date.hour();
    let min = date.minute();
    let sec = date.second();

    offset(secs).ymd(year, month, day).and_hms(hour, min, sec)
}

fn time_rfc2822(secs: i32) -> String {
    time(secs).to_rfc2822()
}

fn time_rfc3339(secs: i32) -> String {
    time(secs).to_rfc3339()
}

fn timestamp() -> String {
    SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_secs()
        .to_string()
}

fn uuid_v4() -> String {
    Uuid::new_v4().to_string()
}

fn word() -> String {
    Word().fake()
}

fn words(min: usize, max: usize) -> String {
    let count = min..(max + 1);
    Words(count).fake::<Vec<_>>().join(" ")
}
use actix_web::{HttpRequest, HttpResponse, route};
use actix_web::web::{Data, Payload};
use tokio::join;
use tracing::{info, warn};

use crate::{
    Incoming,
    IncomingBody,
    Request,
    RequestScope,
    Response,
    ServiceConfig
};

#[route("/{t:.*}", method="DELETE", method="GET", method="HEAD", method="OPTIONS", method="PATCH", method="POST", method="PUT")]
pub(super) async fn factory(request: HttpRequest, payload: Payload, config: Data<ServiceConfig>) -> Response {
    let route_path = format!("{}{}", request.method(), request.path());
    info!("Routing request for {}", route_path);
    if let Some((actix_path, route)) = config.match_at(&route_path) {
        info!("Path {} matched endpoint(s) {}", route_path, route);
        let mut scope = RequestScope { channels: &config.channels, incoming: Incoming::new(&request) };
        let mut body = IncomingBody::Awaited(&request, payload.into_inner());
        for endpoint in route.endpoints() {
            info!("Matching endpoint '{}' to provided expectations", endpoint.name());
            if endpoint.matches(&mut scope, &mut body).await? {
                info!("Endpoint '{}' matches expectations. Generating response", endpoint.name());
                let request = Request::with(actix_path, scope.incoming, body);
                let response = endpoint.response(request, &config).await?;
                let persistent = endpoint.persistent();
                let (store, _) = join!(config.state.persist(persistent), endpoint.delay());
                if let Err(err) = store {
                    warn!("Possible data loss detected. State storage error: {}", err);
                }

                info!("Sending {}", response.status());
                return Ok(response);
            }

            info!("Matching failed for '{}'", endpoint.name())
        }

        info!("No matching endpoints found for {}. Sending 400 BAD_REQUEST", route_path);
        return Ok(HttpResponse::BadRequest().finish());
    }

    info!("No matching route found for {}. Sending 404 NOT_FOUND", route_path);
    Ok(HttpResponse::NotFound().finish())
}
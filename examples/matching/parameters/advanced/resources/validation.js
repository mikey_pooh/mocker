// noinspection JSUnresolvedFunction,JSUnusedGlobalSymbols

const MAX_SPAN = 10000;

// Scripts should return an object representing a map of key to function using an IFFE as below
// All functions are called within the scope of the object returned from this function ('this' is bound to this object)
(function() {
    return {
        // Validation functions should always return a boolean value.
        validateRange: function(value) {
            // Expects a range in the form x-y or x-+y (latter to indicate inclusive range)
            // Logging is available at all normal levels (console.log is a 'duplicate' of console.info)
            const parts = value.split('-', 2);
            if (parts.length === 2) {
                const start = Number.parseInt(parts[0]);
                if (!Number.isNaN(start) && start >= 1) {
                    const endPart = parts[1];
                    const inclusive = (endPart.startsWith('+') && endPart.length > 1);
                    const end = inclusive ? Number.parseInt(endPart.substring(1)) + 1 : Number.parseInt(endPart);
                    if (!Number.isNaN(end) && end > start) {
                        const span = end - start;
                        const isValid = span >= 1 && span <= MAX_SPAN;
                        if (!isValid) {
                            console.log(`Span: '${span}'. Span must be >= 1 and <= 10000.`);
                        }

                        return isValid;
                    }
                }
            }

            console.log(`Invalid range: '${value}'`);
            return false;
        }
    }
})();
// noinspection JSUnresolvedFunction,JSUnusedGlobalSymbols

// Scripts should return an object representing a map of key to function using an IFFE as below
// All functions are called within the scope of the object returned from this function ('this' is bound to this object)
(function() {
    return {
        // Validation functions should always return a boolean value.
        // Logging is available at all normal levels (console.log is a 'duplicate' of console.info)
        validateDisplay: function(value) {
            const isValid = value >= 1000;
            if (!isValid) {
                console.log(`Validation failed: ${value} < 1000`);
            }

            return isValid;
        }
    }
})();
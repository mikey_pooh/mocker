// noinspection JSUnresolvedFunction,JSUnusedGlobalSymbols
(function() {
    return {
        transformBody: function(response) {
            if (response.body) {
                response.body.name = 'Mike Radley';
                response.body.email = 'mike.radley@iliad-solutions.com';
                response.status = 201;
                return response;
            }

            console.error('Body expected but not present.');
            throw { status: 500 };
        }
    }
})();
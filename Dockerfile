FROM rust:1.62.1-alpine

RUN apk add build-base
RUN mkdir -p /mocker

ENV PATH="/mocker/target/release:${PATH}"

WORKDIR /mocker
COPY . .

RUN cargo build --release
CMD mocker ./resources

// noinspection JSUnresolvedFunction

(function() {
    return {
        before: function(_, state) {
            state.count = state.count || 0;
            state.count += 1;

            console.log(`Request count is: ${state.count}`, "passthrough", "default");
        },
        generate: function(request, state) {
            return {
                city: city(),
                count: state.count,
                email: email(),
                firstName: firstName(),
                paragraph: paragraphs(1, 1),
                time_rfc2822: time_rfc2822(3600),
                time_rfc3339: time_rfc3339(-3600)
            };
        }
    }
})();